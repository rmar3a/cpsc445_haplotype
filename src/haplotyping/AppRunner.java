package haplotyping;

import java.awt.BorderLayout;

import haplotyping.presenters.AppController;
import haplotyping.views.uicomponents.CopyRightPanel;
import haplotyping.views.uicomponents.HaploFinderFrame;
import haplotyping.views.uicomponents.HaploFinderMenuBar;

/**
 * Class to run the application.
 */
public class AppRunner
{
	public static void main(String[] args)
	{
		AppController appPresenter = new AppController();
		HaploFinderFrame window = new HaploFinderFrame("HaploFinder");
		window.getContentPane().add(new HaploFinderMenuBar(), BorderLayout.NORTH);
		window.getContentPane().add(new CopyRightPanel(), BorderLayout.SOUTH);
		appPresenter.go(window);
	}
}
