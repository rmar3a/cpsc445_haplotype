package haplotyping.presenters;

import haplotyping.backend.Haplotyper;
import haplotyping.models.InfoBag;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 * Presenter for the PreprocessView.
 */
public class PreprocessPresenter
{
	/**
	 * Contract between the presenter and view that is associated to it.
	 */
	public interface PreprocessDisplay
	{
		/**
		 * Retrieve the Haplotype button.
		 * @pre true;
		 * @post true;
		 * @return the Haplotype button to perform Haplotyping.
		 */
		public AbstractButton getHaplotypeBtn();
		
		/**
		 * Retrieve the Restart button.
		 * @pre true;
		 * @post true;
		 * @return the Restart button.
		 */
		public AbstractButton getRestartBtn();
		
		/**
		 * Display the given information in the InfoBag.
		 * @pre info != null;
		 * @post true;
		 * @param info - the information to display.
		 */
		public void displayPreprocessedData(InfoBag info);
		
		/**
		 * Retrieve the view as a Component to add to the view container.
		 * @pre true;
		 * @post true;
		 * @return the view as a Component.
		 */
		public Component asComponent();
	}
	
	private static final String HAPLOTYPING_MESSAGE = "Haplotyping...";
	private final AppController controller;
	private final PreprocessDisplay view;
	private final InfoBag infoBag;
	
	/**
	 * Constructor for the PreprocessPresenter.
	 * @pre controller != null && view != null;
	 * @post this.controller.equals(controller) && this.view.equals(view);
	 * @param controller - the controller for the application.
	 * @param view - the view associated with the presenter via the PreprocessDisplay
	 * 				 interface.
	 * @param info - the information to display in the PreprocessDisplay.
	 */
	public PreprocessPresenter(AppController controller, PreprocessDisplay view, InfoBag info)
	{
		this.controller = controller;
		this.view = view;
		this.infoBag = info;
		this.view.displayPreprocessedData(info);
	}
	
	/**
	 * Remove all contents of the given JPanel and add the view.
	 * @pre viewContainer != null;
	 * @post true;
	 * @param preprocessPanel - the JPanel to add the view to.
	 */
	public void go(JPanel preprocessPanel)
	{
		this.bind();
		preprocessPanel.removeAll();
		preprocessPanel.add(this.view.asComponent());
	}
	
	/**
	 * Helper method to connect the view to the backend.
	 * @pre true;
	 * @post true;
	 */
	private void bind()
	{
		this.initializeRestartBtn();
		this.initializeHaplotypeBtn();
	}
	
	/**
	 * Helper method to initialize the ActionListener for the restart button.
	 * @pre true;
	 * @post true;
	 */
	private void initializeRestartBtn()
	{
		AbstractButton restartBtn = this.view.getRestartBtn();
		restartBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				controller.switchToFileSelectionView();
			}
		});
	}
	
	/**
	 * Helper method to initialize the ActionListener for the Haplotype button.
	 * @pre true;
	 * @post true;
	 */
	private void initializeHaplotypeBtn()
	{
		AbstractButton haplotypeBtn = this.view.getHaplotypeBtn();
		haplotypeBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// switch to loading view
				controller.switchToLoadingView(HAPLOTYPING_MESSAGE);
				
				// create worker to run in background
				SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>()
				{	
				    @Override
				    public Void doInBackground()
				    {
						haplotypeInformation();
				    	return null;
				    }
				};
				
				worker.execute();
			}
		});
	}
	
	/**
	 * Helper method to Haplotype the VCF files given to the PreprocessPresenter.
	 * @pre true;
	 * @post true;
	 */
	private void haplotypeInformation()
	{
		// to do: use backend to perform haplotyping
		try
		{
			Haplotyper.haplotype(this.infoBag.momFile, this.infoBag.dadFile, this.infoBag.childFiles, this.infoBag.genome);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// switch to Haplotyped view
		this.controller.switchToHaploviewView(this.infoBag);
	}
}
