package haplotyping.presenters;

import java.awt.CardLayout;
import java.util.ArrayList;

import haplotyping.models.InfoBag;
import haplotyping.views.FileSelectionView;
import haplotyping.views.HaploviewView;
import haplotyping.views.LoadingView;
import haplotyping.views.PreprocessView;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Class to carry out all transitions for the application.
 */
public class AppController
{
	public enum View
	{
		FILE_SELECTION, PROGRESS_BAR, PREPROCESS, HAPLOVIEW
	};
	
	private CardLayout view;
	private JFrame viewHolder;
	private JPanel cardPanel;
	private JPanel selectFilePanel;
	private JPanel progressPanel;
	private JPanel preprocessPanel;
	private JPanel haploviewPanel;
	
	/**
	 * Empty constructor for AppController.
	 * @pre true;
	 * @post true;
	 */
	public AppController()
	{
		this.initializeBasePanels();
	}
	
	/**
	 * Helper method to initialize the base panels for each view.
	 * @pre true;
	 * @post true;
	 */
	private void initializeBasePanels()
	{
		 this.view = new CardLayout();
         this.cardPanel = new JPanel(this.view);
         this.selectFilePanel = new JPanel();
         this.progressPanel = new JPanel();
         this.preprocessPanel = new JPanel();
         this.haploviewPanel = new JPanel();
         
         this.cardPanel.add(this.selectFilePanel, View.FILE_SELECTION.toString());
         this.cardPanel.add(this.progressPanel, View.PROGRESS_BAR.toString());
         this.cardPanel.add(this.preprocessPanel, View.PREPROCESS.toString());
         this.cardPanel.add(this.haploviewPanel, View.HAPLOVIEW.toString());
	}
	
	/**
	 * Initialize the application.
	 * @pre true;
	 * @post true;
	 */
	public void go(JFrame viewHolder)
	{
		this.viewHolder = viewHolder;
		viewHolder.add(this.cardPanel);
		this.bind();
	}
	
	/**
	 * Helper method to initialize the application, forming the connections
	 * between the UI and backend.
	 * @pre true;
	 * @post true;
	 */
	private void bind()
	{
		FileSelectionPresenter fileSelectionPresenter = new FileSelectionPresenter(this, new FileSelectionView());
		fileSelectionPresenter.go(this.selectFilePanel);
		this.presentView();
	}

	/**
	 * Switch to the file selection view; i.e. the default view.
	 * @pre true;
	 * @post true;
	 */
	public void switchToFileSelectionView()
	{
		FileSelectionPresenter fileSelectionPresenter = new FileSelectionPresenter(this, new FileSelectionView());
		fileSelectionPresenter.go(this.selectFilePanel);
		this.presentView();
		
		this.view.show(this.cardPanel, View.FILE_SELECTION.toString());
	}
	
	/**
	 * Switch to the file selection view; i.e. the default view, prepopulating with the given file paths and error
	 * message if one needs to be displayed.
	 * @pre children != null && moms != null;
	 * @post true;
	 * @param children - the list of file paths for the chidren VCF files.
	 * @param moms - the list of file paths for the mom VCF files.
	 * @param errorMessage - the error message to display; if set to null, then no error message displayed.
	 */
	public void switchToFileSelectionView(ArrayList<String> children, ArrayList<String> moms, String errorMessage)
	{
		FileSelectionPresenter fileSelectionPresenter = new FileSelectionPresenter(this, new FileSelectionView(children, moms, errorMessage));
		fileSelectionPresenter.go(this.selectFilePanel);
		this.presentView();
		
		this.view.show(this.cardPanel, View.FILE_SELECTION.toString());
	}
	
	/**
	 * Switch to the loading view displaying the progress bar.
	 * @pre true;
	 * @post true;
	 * @param loadingMessage - loading message to display.
	 */
	public void switchToLoadingView(String loadingMessage)
	{		
		LoadingPresenter loadingPresenter = new LoadingPresenter(new LoadingView(loadingMessage));
		loadingPresenter.go(this.progressPanel);
		this.presentView();
		
		this.view.show(this.cardPanel, View.PROGRESS_BAR.toString());
	}
	
	/**
	 * Switch to the preprocessed view, displaying the given VCFInfo objects.
	 * @pre momFiles != null && childFiles != null && dadFile != null;
	 * @post true;
	 * @param info - the InfoBag containing the preprocessed VCF information to display.
	 */
	public void switchToPreprocessView(InfoBag info)
	{
		PreprocessPresenter preprocessPresenter = new PreprocessPresenter(this, new PreprocessView(), info);
		preprocessPresenter.go(this.preprocessPanel);
		this.presentView();
		
		this.view.show(this.cardPanel, View.PREPROCESS.toString());
	}
	
	/**
	 * Switch to the Haploview view, displaying the imputed Haplotypes.
	 * @pre vcfFiles != null;
	 * @post true;
	 * @param info - the InfoBag containing the haplotyped VCF information to display.
	 */
	public void switchToHaploviewView(InfoBag info)
	{
		HaploviewPresenter haploviewPresenter = new HaploviewPresenter(this, new HaploviewView(), info);
		haploviewPresenter.go(this.haploviewPanel);
		this.presentView();
		
		this.view.show(this.cardPanel, View.HAPLOVIEW.toString());
	}
	
	/**
	 * Helper method to pack the view and centre it.
	 * @pre true;
	 * @post true;
	 */
	private void presentView()
	{
		this.viewHolder.pack();
		this.viewHolder.setLocationRelativeTo(null);
	}
}
