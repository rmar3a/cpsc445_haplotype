package haplotyping.presenters;

import haplotyping.models.InfoBag;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JPanel;

/**
 * Presenter for the HaploviewView.
 */
public class HaploviewPresenter
{
	/**
	 * Contract between the presenter and view that is associated to it.
	 */
	public interface HaploviewDisplay
	{
		/**
		 * Retrieve the Restart button.
		 * @pre true;
		 * @post true;
		 * @return the Restart button.
		 */
		public AbstractButton getRestartBtn();
		
		/**
		 * Display the given information in the InfoBag.
		 * @pre info != null;
		 * @post true;
		 * @param info - the information to display.
		 */
		public void displayHaplotypedData(InfoBag info);
		
		/**
		 * Retrieve the view as a Component to add to the view container.
		 * @pre true;
		 * @post true;
		 * @return the view as a Component.
		 */
		public Component asComponent();
	}
	
	private final AppController controller;
	private final HaploviewDisplay view;
	
	/**
	 * Contructor for the HaploviewPresenter.
	 * @pre controller != null && view != null;
	 * @post true;
	 * @param controller - the controller for the application.
	 * @param view - the view associated with the presenter via the HaploviewDisplay
	 * 				 interface.
	 * @param info - the information to display in the HaploviewDisplay.
	 */
	public HaploviewPresenter(AppController controller, HaploviewDisplay view, InfoBag info)
	{
		this.controller = controller;
		this.view = view;
		this.view.displayHaplotypedData(info);
	}
	
	/**
	 * Remove all contents of the given JPanel and add the view.
	 * @pre viewContainer != null;
	 * @post true;
	 * @param preprocessPanel - the JPanel to add the view to.
	 */
	public void go(JPanel preprocessPanel)
	{
		this.bind();
		preprocessPanel.removeAll();
		preprocessPanel.add(this.view.asComponent());
	}
	
	/**
	 * Helper method to connect the view to the backend.
	 * @pre true;
	 * @post true;
	 */
	private void bind()
	{
		this.initializeRestartBtn();
	}
	
	/**
	 * Helper method to initialize the ActionListener for the restart button.
	 * @pre true;
	 * @post true;
	 */
	private void initializeRestartBtn()
	{
		AbstractButton restartBtn = this.view.getRestartBtn();
		restartBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				controller.switchToFileSelectionView();
			}
		});
	}
}
