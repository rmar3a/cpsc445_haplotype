package haplotyping.presenters;

import haplotyping.backend.BulkVCFParser;
import haplotyping.backend.Preprocessor;
import haplotyping.backend.VCFParser;
import haplotyping.models.Genome;
import haplotyping.models.InfoBag;
import haplotyping.models.VCFInfo;
import haplotyping.models.VCFInfo.Relation;
import haplotyping.views.uicomponents.FileChooser;
import haplotyping.views.uicomponents.FileChooser.Filter;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 * Presenter for the FileSelectionView.
 */
public class FileSelectionPresenter
{
	/**
	 * Contract between the presenter and view that is associated to it.
	 */
	public interface FileSelectionDisplay
	{
		/**
		 * Get the pre-process button.
		 * @pre true;
		 * @post true;
		 * @return the "button" that will have the pre-process function to be connected to it.
		 */
		public AbstractButton getPreprocessBtn();
		
		/**
		 * Get the bulk import button.
		 * @pre true;
		 * @post true;
		 * @return the "button" that will have the bulk import function to be connected to it.
		 */
		public AbstractButton getBulkImportBtn();
		
		/**
		 * Display the file paths from the bulk import list.
		 * @pre true;
		 * @post true;
		 * @param momFilePaths - ArrayList containing the file paths to mom VCF files.
		 * @param childFilePaths - ArrayList containing the file paths to child VCF files.
		 */
		public void displayBulkImportFiles(ArrayList<String> momFilePaths, ArrayList<String> childFilePaths);
		
		/**
		 * Retrieve the file paths to the mom VCF files selected.
		 * @pre true;
		 * @post true;
		 * @return the absolute file paths to the mom VCF files selected.
		 */
		public ArrayList<String> getSelectedMomFiles();
		
		/**
		 * Retrieve the file paths to the child VCF files selected.
		 * @pre true;
		 * @post true;
		 * @return the absolute file paths to the child VCF files selected.
		 */
		public ArrayList<String> getSelectedChildFiles();
		
		/**
		 * Determine if files have been selected to preprocess; makes error message
		 * appear on display if something is wrong.
		 * @pre true;
		 * @post true;
		 * @return true if files have been selected; false otherwise.
		 */
		public boolean hasValidFiles();
		
		/**
		 * Retrieve the view as a Component to add to the view container.
		 * @pre true;
		 * @post true;
		 * @return the view as a Component.
		 */
		public Component asComponent();
	}

	private static final String PREPROCESS_MESSAGE = "Preprocessing files...";
	private static final String INVALID_FILE_MESSAGE = "Invalid file paths were provided, please double check the selected files.";
	private final BulkVCFParser bulkFileImporter;
	private final AppController controller;
	private final FileSelectionDisplay view;
	
	/**
	 * Constructor for the FileSelectionPresenter.
	 * @pre true;
	 * @post true;
	 * @param controller - the controller for the application.
	 * @param view - the view associated with the presenter via the
	 * 				 FileSelectionDisplay interface.
	 */
	public FileSelectionPresenter(AppController controller, FileSelectionDisplay view)
	{
		this.bulkFileImporter = new BulkVCFParser();
		this.controller = controller;
		this.view = view;
	}
	
	/**
	 * Remove all contents of the given JFrame and add the view.
	 * @pre viewContainer != null;
	 * @post true;
	 * @param selectFilePanel - the JFrame to add the view to.
	 */
	public void go(JPanel selectFilePanel)
	{
		this.bind();
		selectFilePanel.removeAll();
		selectFilePanel.add(this.view.asComponent());
	}
	
	/**
	 * Helper method to connect the view to the backend.
	 * @pre true;
	 * @post true;
	 */
	private void bind()
	{
		this.initializePreprocessBtn();
		this.initializeBulkImportBtn();
	}
	
	/**
	 * Helper method to initialize the ActionListener for the start button.
	 * @pre true;
	 * @post true;
	 */
	private void initializePreprocessBtn()
	{
		AbstractButton startBtn = this.view.getPreprocessBtn();
		startBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(view.hasValidFiles())
				{
					// switch to loading view
					controller.switchToLoadingView(PREPROCESS_MESSAGE);
					
					// create worker to run in background
					SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>()
					{
					    @Override
					    public Void doInBackground()
					    {
							processInformation();
					    	return null;
					    }
					};
					
					worker.execute();
				}
			}
		});
	}
	
	/**
	 * Helper method to retrieve the file paths from the view and preprocess the
	 * information in the VCF files.
	 * @pre true;
	 * @post true;
	 */
	private void processInformation()
	{
		// retrieve the file paths from the UI
		ArrayList<String> children = view.getSelectedChildFiles();
		ArrayList<String> moms = view.getSelectedMomFiles();
		ArrayList<VCFInfo> momFiles = new ArrayList<VCFInfo>();
		ArrayList<VCFInfo> childFiles = new ArrayList<VCFInfo>();
		
		Genome sunflowerGenome = new Genome(Genome.DIPLOID_N);
		boolean fileReadingSuccess = true;
		
		try
		{
			// create VCFInfo objects from the VCF files
			for(String child : children)
			{
				childFiles.addAll(VCFParser.extractInfo(child, sunflowerGenome, Relation.CHILD));
			}
			
			for(String mom : moms)
			{
				momFiles.addAll(VCFParser.extractInfo(mom, sunflowerGenome, Relation.MOM));
			}
		}
		catch(Exception ex)
		{
			controller.switchToFileSelectionView(children, moms, INVALID_FILE_MESSAGE);
			fileReadingSuccess = false;
		}
		
		if(fileReadingSuccess)
		{
			VCFInfo imputedDadVCFFile = new VCFInfo();
			VCFInfo imputedMomVCFFile = new VCFInfo();
			imputedDadVCFFile.setRelation(Relation.DAD);
			imputedDadVCFFile.setIndividualID("Imputed Dad");
			imputedMomVCFFile.setRelation(Relation.MOM);
			imputedMomVCFFile.setIndividualID("Imputed Mom");
			
			// preprocess data
			Preprocessor preprocessor = new Preprocessor(sunflowerGenome);
			
			try
			{
				preprocessor.imputeParentGenotype(momFiles, imputedDadVCFFile, imputedMomVCFFile, childFiles);
				
				// load next view
				controller.switchToPreprocessView(new InfoBag(childFiles, imputedDadVCFFile, imputedMomVCFFile, sunflowerGenome));
			}
			catch(Exception ex)
			{
				String errorMessage = ex.getMessage();
				
				if(errorMessage == null)
				{
					errorMessage = "Unknown error occurred.";
				}
				
				controller.switchToFileSelectionView(children, moms, errorMessage);
			}
		}
	}
	
	/**
	 * Helper method to intialize the ActionListener for the bulk import button.
	 * @pre true;
	 * @post true;
	 */
	private void initializeBulkImportBtn()
	{	
		AbstractButton bulkImportBtn = this.view.getBulkImportBtn();
		bulkImportBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				FileChooser fileChooser = new FileChooser("Select Bulk Import File", null, Filter.TXT);
				File choosenFile = fileChooser.getSelectedFile();
				
				// if file selected display the file path
				if(fileChooser.getButtonNumber() == JFileChooser.APPROVE_OPTION)
				{
					bulkFileImporter.parseBulkFile(choosenFile.getAbsolutePath());
					view.displayBulkImportFiles(bulkFileImporter.getMomFiles(), bulkFileImporter.getChildFiles());
				}
			}
		});
	}
}
