package haplotyping.presenters;

import java.awt.Component;

import javax.swing.JPanel;

/**
 * Presenter for the LoadingView
 */
public class LoadingPresenter
{
	/**
	 * Contract between the presenter and view that is associated to it.
	 */
	public interface LoadingDisplay
	{
		/**
		 * Retrieve the view as a Component to add to the view container.
		 * @pre true;
		 * @post true;
		 * @return the view as a Component.
		 */
		public Component asComponent();
	}
	
	private final LoadingDisplay view;
	
	/**
	 * Constructor for the LoadingPresenter.
	 * @pre view != null;
	 * @post this.view.equals(view);
	 */
	public LoadingPresenter(LoadingDisplay view)
	{
		this.view = view;
	}
	
	/**
	 * Remove all contents of the given JPanel and add the view.
	 * @pre viewContainer != null;
	 * @post true;
	 * @param progressPanel - the JPanel to add the view to.
	 */
	public void go(JPanel progressPanel)
	{
		progressPanel.removeAll();
		progressPanel.add(this.view.asComponent());
	}
}
