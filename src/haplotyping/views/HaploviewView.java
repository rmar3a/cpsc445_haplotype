package haplotyping.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import haplotyping.models.InfoBag;
import haplotyping.models.VCFInfo;
import haplotyping.presenters.HaploviewPresenter.HaploviewDisplay;
import haplotyping.views.uicomponents.HaplotypedPanel;

/**
 * Display the VCFInfo objects after the Haplotypes have been imputed.
 */
public class HaploviewView extends JPanel implements HaploviewDisplay
{
	private static final int DISPLAY_WIDTH = 760;
	private static final int DISPLAY_HEIGHT = 500;
	private JButton restartBtn;
	private JPanel individualsHolder;
	
	/**
	 * Constructor for the HaploviewView.
	 * @pre true;
	 * @post true;
	 */
	public HaploviewView()
	{
		this.initializeUI();
	}
	
	/**
	 * Helper method to initilaize the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		individualsHolder = new JPanel();
		individualsHolder.setLayout(new BoxLayout(individualsHolder, BoxLayout.Y_AXIS)); 
		JPanel btnHolder = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JScrollPane scrollView = new JScrollPane(individualsHolder);
		scrollView.setPreferredSize(new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT));
		
		this.addHierarchyBoundsListener(new HierarchyBoundsAdapter()
		{	
			@Override
			public void ancestorResized(HierarchyEvent arg0)
			{
				setPreferredSize(getParent().getSize());
			}
		});
		
		this.restartBtn = new JButton("Restart");
		
		btnHolder.add(this.restartBtn);
		
		this.add(scrollView);
		this.add(btnHolder);
	}
	
	@Override
	public AbstractButton getRestartBtn()
	{
		return this.restartBtn;
	}
	
	@Override
	public void displayHaplotypedData(InfoBag info)
	{
		this.individualsHolder.removeAll();
		this.individualsHolder.add(new HaplotypedPanel(info.dadFile, info.genome));
		this.individualsHolder.add(new HaplotypedPanel(info.momFile, info.genome));
		
		for(VCFInfo file : info.childFiles)
		{
			this.individualsHolder.add(new HaplotypedPanel(file, info.genome));
		}
	}
	
	@Override
	public Component asComponent()
	{
		return this;
	}

	/**
	 * Serial version for the HaploviewView.
	 */
	private static final long serialVersionUID = -5379561920522770757L;
}
