package haplotyping.views;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import haplotyping.presenters.FileSelectionPresenter.FileSelectionDisplay;
import haplotyping.views.uicomponents.FamilySelector;
import haplotyping.views.uicomponents.FamilySelector.Relation;
import haplotyping.views.uicomponents.MessagePanel;

/**
 * Display for the file selection view.
 */
public class FileSelectionView extends JPanel implements FileSelectionDisplay
{
	private static final int VCF_SELECTOR_WIDTH = 760;
	private static final int VCF_SELECTOR_HEIGHT = 500;
	
	private AbstractButton startAlleleFinderBtn;
	private AbstractButton bulkImportBtn;
	private FamilySelector selectorDisplay;
	private MessagePanel errorDisplay;
	
	/**
	 * Constructor for FileSelectionView view.
	 * @pre windowTitle != null;
	 * @post true;
	 */
	public FileSelectionView()
	{
		super();
		this.initializeUI();
	}
	
	/**
	 * Constructor for FileSelectionView view, prepopulating it with the given file paths and error
	 * message if provided.
	 * @pre children != null && moms != null;
	 * @post true;
	 * @param children - the list of file paths to children VCF files.
	 * @param moms - the list of file paths to moms VCF files.
	 * @param errorMessage - the error message to display; if set to null, then no error message displayed.
	 */
	public FileSelectionView(ArrayList<String> children, ArrayList<String> moms, String errorMessage)
	{
		super();
		this.initializeUI();
		this.selectorDisplay.loadFilePaths(children, Relation.CHILD);
		this.selectorDisplay.loadFilePaths(moms, Relation.MOM);
		
		if(errorMessage != null)
		{
			this.errorDisplay.displayErrorMessage(errorMessage);
		}
	}
	
	/**
	 * Helper method to initialize the UI for the view.
	 * @pre true;
	 * @post this.isVisible() == true;
	 */
	private void initializeUI()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.startAlleleFinderBtn = new JButton("Preprocess");
		this.bulkImportBtn = new JButton("Bulk Import");
		this.selectorDisplay = new FamilySelector();
		this.errorDisplay = new MessagePanel(null);
		
		JPanel topBtnHolder = new JPanel(new FlowLayout());
		JPanel bottomBtnHolder = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JPanel motherBtnHolder = new JPanel();
		JPanel childBtnHolder = new JPanel();
		
		JButton addMother = new JButton("Add Mother");
		JButton addChild = new JButton("Add Child");
		JButton resetMother = new JButton("Reset Mother");
		JButton resetChild = new JButton("Reset Child");
		
		addMother.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				selectorDisplay.addMother();
			}
		});
		
		addChild.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				selectorDisplay.addChild();
			}
		});
		
		resetMother.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				selectorDisplay.resetMother();
			}
		});
		
		resetChild.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				selectorDisplay.resetChild();
			}
		});
		
		JScrollPane scrollView = new JScrollPane(selectorDisplay);
		scrollView.setPreferredSize(new Dimension(VCF_SELECTOR_WIDTH, VCF_SELECTOR_HEIGHT));
		
		this.addHierarchyBoundsListener(new HierarchyBoundsAdapter()
		{	
			@Override
			public void ancestorResized(HierarchyEvent arg0)
			{
				setPreferredSize(getParent().getSize());
			}
		});
		
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        JPanel title = new JPanel(flowLayout);
        title.add(new JLabel("Selected mom VCF files:"));
        title.add(Box.createHorizontalStrut(185));
        title.add(new JLabel("Selected child VCF files:"));
        title.add(Box.createHorizontalStrut(170));
		
		this.add(title);
		this.add(scrollView);
		
		motherBtnHolder.add(addMother);
		childBtnHolder.add(addChild);
		motherBtnHolder.add(resetMother);
		childBtnHolder.add(resetChild);
		
		topBtnHolder.add(motherBtnHolder);
		topBtnHolder.add(Box.createHorizontalStrut(140));
		topBtnHolder.add(childBtnHolder);
		topBtnHolder.add(Box.createHorizontalStrut(85));

		bottomBtnHolder.add(this.errorDisplay);
		bottomBtnHolder.add(Box.createHorizontalStrut(85));
		bottomBtnHolder.add(bulkImportBtn);
		bottomBtnHolder.add(this.startAlleleFinderBtn);
		
		this.add(topBtnHolder);
		this.add(bottomBtnHolder);
		this.setVisible(true);
	}
	
	@Override
	public boolean hasValidFiles()
	{
		ArrayList<String> childFiles = this.selectorDisplay.getFilePaths(Relation.CHILD);
		ArrayList<String> momFiles = this.selectorDisplay.getFilePaths(Relation.MOM);
		
		boolean hasMomFiles = this.hasFiles(momFiles);
		boolean hasChildFiles = this.hasFiles(childFiles);
		
		String message;
		
		if(!hasMomFiles && !hasChildFiles)
		{
			message = "You must select Child and Mom VCF files to preprocess.";
			this.errorDisplay.displayErrorMessage(message);
		}
		else if(!hasMomFiles)
		{
			message = "You must select Mom VCF files to preprocess.";
			this.errorDisplay.displayErrorMessage(message);
		}
		else if(!hasChildFiles)
		{
			message = "You must select Child VCF files to preprocess.";
			this.errorDisplay.displayErrorMessage(message);
		}

		return hasMomFiles && hasChildFiles;
	}
	
	/**
	 * Helper method to determine if a give list of strings contains any empty values.
	 * @pre fileList != null;
	 * @post true;
	 * @param fileList - the list of file paths to check.
	 * @return true if there are any file paths; false otherwise.
	 */
	private boolean hasFiles(ArrayList<String> fileList)
	{	
		for(String file : fileList)
		{
			if(!file.trim().equals(""))
			{
				return true;
			}
		}
			
		return false;
	}
	
	@Override
	public void displayBulkImportFiles(ArrayList<String> momFilePaths, ArrayList<String> childFilePaths)
	{
		this.selectorDisplay.loadFilePaths(momFilePaths, Relation.MOM);
		this.selectorDisplay.loadFilePaths(childFilePaths, Relation.CHILD);
	}
	
	@Override
	public ArrayList<String> getSelectedChildFiles()
	{
		return this.selectorDisplay.getFilePaths(Relation.CHILD);
	}
	
	@Override
	public ArrayList<String> getSelectedMomFiles()
	{
		return this.selectorDisplay.getFilePaths(Relation.MOM);
	}
	
	@Override
	public AbstractButton getPreprocessBtn()
	{
		return this.startAlleleFinderBtn;
	}

	@Override
	public AbstractButton getBulkImportBtn()
	{
		return this.bulkImportBtn;
	}
	
	@Override
	public Component asComponent()
	{
		return this;
	}
	
	/**
	 * Serial version for FileSelectionView.
	 */
	private static final long serialVersionUID = 1156818383080697463L;
}
