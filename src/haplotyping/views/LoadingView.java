package haplotyping.views;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import haplotyping.presenters.LoadingPresenter.LoadingDisplay;

/**
 * Class to display the progress bar.
 */
public class LoadingView extends JPanel implements LoadingDisplay
{
	/**
	 * Contructor for the LoadingView.
	 * @pre true;
	 * @post true;
	 * @param progressMessage - the message displayed above the progress bar.
	 */
	public LoadingView(String progressMessage)
	{
		this.initializeUI(progressMessage);
	}
	
	/**
	 * Helper method to initialize the UI.
	 * @pre true;
	 * @post true;
	 * @param progressMessage - the message displayed above the progress bar.
	 */
	private void initializeUI(String progressMessage)
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel progressTitle = new JLabel(progressMessage);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		
		this.add(Box.createVerticalStrut(300));
		this.add(progressTitle);
		this.add(progressBar);
	}
	
	@Override
	public Component asComponent()
	{
		return this;
	}

	/**
	 * Serial version for the LoadingView.
	 */
	private static final long serialVersionUID = 6925018102591294984L;	
}
