package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of Copy Right label.
 */
public class HighlightColour extends Color
{
	private static final int RED = 0;
	private static final int GREEN = 184;
	private static final int BLUE = 107;
	
	/**
	 * Constructor for the HighlightColour.
	 * @pre true;
	 * @post true;
	 */
	public HighlightColour()
	{
		super(RED, GREEN, BLUE);
	}

	/**
	 * Serial version of HighlightColour
	 */
	private static final long serialVersionUID = -3696502799124681143L;
}
