package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of missing data.
 */
public class MissingDataColour extends Color
{
	private static final int RED = 255;
	private static final int GREEN = 59;
	private static final int BLUE = 114;
	
	/**
	 * Constructor for the MissingDataColour.
	 * @pre true;
	 * @post true;
	 */
	public MissingDataColour()
	{
		super(RED, GREEN, BLUE);
	}
	
	/**
	 * Serial version for the MissingDataColour.
	 */
	private static final long serialVersionUID = 1214377248937753213L;
}
