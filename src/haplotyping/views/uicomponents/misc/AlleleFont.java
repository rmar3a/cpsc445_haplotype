package haplotyping.views.uicomponents.misc;

import java.awt.Font;

/**
 * Custom mono-space font for displaying alleles.
 */
public class AlleleFont extends Font
{
	private static final String FONT = "Courier";
	private static final int FONT_TYPE = Font.PLAIN;
	private static final int FONT_SIZE = 14;
	
	/**
	 * Constructor for the AlleleFont.
	 * @pre true;
	 * @post true;
	 */
	public AlleleFont()
	{
		super(FONT, FONT_TYPE, FONT_SIZE);
	}
	
	/**
	 * Serial version for AlleleFont.
	 */
	private static final long serialVersionUID = 8963164849750448680L;
}
