package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for error messages.
 */
public class ErrorMessageColour extends Color
{
	private static final int RED = 143;
	private static final int GREEN = 0;
	private static final int BLUE = 56;
	
	/**
	 * Constructor for the ErrorInputBoxColour.
	 * @pre true;
	 * @post true;
	 */
	public ErrorMessageColour()
	{
		super(RED, GREEN, BLUE);
	}
	
	/**
	 * Serial version for the ErrorMessageColour.
	 */
	private static final long serialVersionUID = -5761159721399705805L;
}
