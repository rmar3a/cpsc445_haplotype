package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of Copy Right label.
 */
public class CopyRightColour extends Color
{
	private static final int RED = 128;
	private static final int GREEN = 128;
	private static final int BLUE = 128;
	
	/**
	 * Constructor for the CopyRightColour.
	 * @pre true;
	 * @post true;
	 */
	public CopyRightColour()
	{
		super(RED, GREEN, BLUE);
	}

	/**
	 * Serial version of CopyRightColour.
	 */
	private static final long serialVersionUID = 4487869573983977759L;
}
