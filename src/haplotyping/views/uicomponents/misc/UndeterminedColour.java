package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of undetermined alleles.
 */
public class UndeterminedColour extends Color
{
	private static final int RED = 209;
	private static final int GREEN = 122;
	private static final int BLUE = 0;
	
	/**
	 * Constructor for the MomAlleleColour.
	 * @pre true;
	 * @post true;
	 */
	public UndeterminedColour()
	{
		super(RED, GREEN, BLUE);
	}
	
	/**
	 * Serial version for the UndeterminedColour.
	 */
	private static final long serialVersionUID = 2062230777653066699L;
}
