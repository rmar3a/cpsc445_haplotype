package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of dad allele.
 */
public class DadAlleleColour extends Color
{
	private static final int RED = 117;
	private static final int GREEN = 193;
	private static final int BLUE = 225;
	
	/**
	 * Constructor for the DadAlleleColour.
	 * @pre true;
	 * @post true;
	 */
	public DadAlleleColour()
	{
		super(RED, GREEN, BLUE);
	}
	
	/**
	 * Serial version of the DadAlleleColour.
	 */
	private static final long serialVersionUID = -8853994960944305051L;
}
