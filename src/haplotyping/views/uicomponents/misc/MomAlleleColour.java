package haplotyping.views.uicomponents.misc;

import java.awt.Color;

/**
 * Custom colour for background of dad allele.
 */
public class MomAlleleColour extends Color
{
	private static final int RED = 212;
	private static final int GREEN = 212;
	private static final int BLUE = 0;
	
	/**
	 * Constructor for the MomAlleleColour.
	 * @pre true;
	 * @post true;
	 */
	public MomAlleleColour()
	{
		super(RED, GREEN, BLUE);
	}

	/**
	 * Serial version for the MomAlleleColour.
	 */
	private static final long serialVersionUID = 2194264046182499037L;
}
