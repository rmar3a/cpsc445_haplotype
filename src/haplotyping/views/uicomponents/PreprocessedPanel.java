package haplotyping.views.uicomponents;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.VCFInfo;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class to display a VCFInfo object that has been preprocessed.
 */
public class PreprocessedPanel extends JPanel
{
	private JPanel individualInfo;
	
	/**
	 * Constructor for the PreprocessedPanel.
	 * @pre vcfFile != null;
	 * @post true;
	 * @param vcfFile - the VCF file information for the preprocessed individual.
	 * @param genome - the genome for the given VCF file.
	 */
	public PreprocessedPanel(VCFInfo vcfFile, Genome genome)
	{
		super();
		this.initializeUI(vcfFile, genome);
	}
	
	/**
	 * Helper method to display the information of the given VCF file.
	 * @pre file != null;
	 * @post true;
	 * @param file - the VCFInfo to display.
	 * @param genome - the genome for the given VCF file.
	 */
	private void initializeUI(VCFInfo file, Genome genome)
	{
		this.individualInfo = new JPanel();
		this.individualInfo.setLayout(new BoxLayout(this.individualInfo, BoxLayout.Y_AXIS));
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setAlignmentX(RIGHT_ALIGNMENT);
		this.individualInfo.add(new JLabel(" Relation: " + file.getRelation()));
		
		String individualID = file.getIndividualID();
		
		if(individualID.contains("/"))
		{
			String[] values = individualID.split("/");
			individualID = values[values.length - 1];
		}
		
		this.individualInfo.add(new JLabel(" Ind ID: " + individualID));
		this.add(this.individualInfo);
		this.add(Box.createHorizontalStrut(10));
		
		for(Locus locus : genome.getSortedLoci())
		{
			AlleleInfo alleleInfo = file.getLocusAlleleInfo(locus);
			int maxAlleleSize = genome.getMaxAlleleSize(locus);
			this.add(new LocusPanel(locus, alleleInfo, maxAlleleSize));
		}
	}
		
	/**
	 * Serial version for the PreprocessedPanel.
	 */
	private static final long serialVersionUID = -5819532864097706248L;
}
