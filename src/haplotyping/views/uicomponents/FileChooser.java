package haplotyping.views.uicomponents;

import javax.swing.JComponent;
import javax.swing.JFileChooser;

/**
 * Class for displaying the file chooser for VCF files.
 */
public class FileChooser extends JFileChooser
{
	public enum Filter {VCF, TXT}
	
	private int choiceNumber;
	
	/**
	 * Constructor for the VCFFileChooser.
	 * @pre title != null && location != null && fileType != null;
	 * @post true;
	 * @param title - the text that will appear in the select button of the
	 * 				  file chooser.
	 * @param location - the parent component of the file chooser.
	 * @param fileType - the type of file filter to apply.
	 */
	public FileChooser(String title, JComponent location, Filter fileType)
	{
		super();
		this.setFilter(fileType);
		this.choiceNumber = this.showDialog(location, title);
    	this.setVisible(true);
	}
	
	/**
	 * Helper method to set the file filter for the file chooser.
	 * @pre fileType != null;
	 * @post true;
	 * @param fileType - the type of file filter for the file chooser.
	 */
	private void setFilter(Filter fileType)
	{
		String fileExtension;
		
		if(fileType.equals(Filter.VCF))
		{
			fileExtension = ".vcf";
		}
		else
		{
			fileExtension = ".txt";
		}
		
		this.setFileFilter(new CustomFileFilter(fileExtension));
	}
	
	/**
	 * Returns the value of the selected button from the filechooser dialog.
	 * @pre true;
	 * @post true;
	 * @return the value of the button selected from the filechooser dialog.
	 */
	public int getButtonNumber()
	{
		return this.choiceNumber;
	}
	
	/**
	 * Serial version for the VCFFileChooser.
	 */
	private static final long serialVersionUID = 3080848993723740991L;
}
