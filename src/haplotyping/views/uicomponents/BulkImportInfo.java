package haplotyping.views.uicomponents;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

/**
 * Display the information to explain the format of the bulk import file.
 */
public class BulkImportInfo extends HaploFinderFrame
{
	private static int WIDTH = 280;
	private static int HEIGHT = 280;
	private static String BODY = 
	
	"<HTML><H3>Required format of the text file:</H3>" +
	"Path<br>" +
	"&lt;path_to_VCF_files&gt;<br><br>" +
	"Moms<br>" +
	"&lt;VCF_filename_for_mom_1&gt;<br>" +
	"...<br>" +
	"&lt;VCF_filename_for_mom_n&gt;<br><br>" +
	"Children<br>" +
	"&lt;VCF_filename_for_child_1&gt;<br>" +
	"...<br>" +
	"&lt;VCF_filename_for_child_n&gt;";
	
	/**
	 * Constructor for the BulkImportInfo.
	 * @pre true;
	 * @post true;
	 */
	public BulkImportInfo()
	{
		super("Bulk Import Information");
		this.initializeUI();
	}
	
	/**
	 * Helper method to initialize the BulkImportInfo window.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		JLabel messageBody = new JLabel(BODY);
		Border paddingBorder = BorderFactory.createEmptyBorder(0,15,10,15);	 
		messageBody.setBorder(paddingBorder);
		this.add(messageBody);
		this.pack();
		this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
	}
	
	/**
	 * Serial version for the BulkImportInfo.
	 */
	private static final long serialVersionUID = 8926092251827540943L;

}
