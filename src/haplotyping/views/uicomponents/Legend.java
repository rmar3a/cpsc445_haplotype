package haplotyping.views.uicomponents;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import haplotyping.views.uicomponents.misc.DadAlleleColour;
import haplotyping.views.uicomponents.misc.MissingDataColour;
import haplotyping.views.uicomponents.misc.MomAlleleColour;
import haplotyping.views.uicomponents.misc.UndeterminedColour;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Displays the legend for the output produced from preprocessing and haplotyping
 * the VCF files.
 */
public class Legend extends HaploFinderFrame
{
	private static final String MISSING_TEXT = "--";
	private static final String PLACE_HOLDER = "**";
	private static final String QUESTION_MARK = "?";
	private static final String UNDETERIMINED_DESCRIPTION = "Unable to determine.";
	private static final String DAD_DESCRIPTION = "Haplotype from Dad.";
	private static final String MOM_DESCRIPTION = "Haplotype from Mom.";
	private static final String MISSING_DESCRIPTION = "Missing data.";
	private static int WIDTH = 200;
	private static int HEIGHT = 136;
	
	/**
	 * Constructor for the Legend.
	 * @pre true;
	 * @post true;
	 */
	public Legend()
	{
		super("Legend");
		this.initializeUI();
	}
	
	/**
	 * Helper method to initialize the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		this.add(new DescriptionPanel(new MissingDataColour(), MISSING_TEXT, MISSING_DESCRIPTION));
		this.add(new DescriptionPanel(new UndeterminedColour(), QUESTION_MARK, UNDETERIMINED_DESCRIPTION));
		this.add(new DescriptionPanel(new DadAlleleColour(), PLACE_HOLDER, DAD_DESCRIPTION));
		this.add(new DescriptionPanel(new MomAlleleColour(), PLACE_HOLDER, MOM_DESCRIPTION));
		
		this.pack();
		this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
	}
	
	/**
	 * Inner class for displaying the content in the legend window.
	 */
	public class DescriptionPanel extends JPanel
	{
		private static final int PADDING = 10;
		
		/**
		 * Constructor for the DescriptionPanel.
		 * @pre bgColour != null && exampleText != null && description != null;
		 * @post true;
		 * @param bgColour - the background colour to display for the example text.
		 * @param exampleText - the text to show with the background colour.
		 * @param description - the description of what the given colour signifies.
		 */
		public DescriptionPanel(Color bgColour, String exampleText, String description)
		{
			this.initializeUI(bgColour, exampleText, description);
		}

		/**
		 * Helper method to initialize the DescriptionPanel.
		 * @pre bgColour != null && exampleText != null && description != null;
		 * @post true;
		 * @param bgColour - the background colour to display for the example text.
		 * @param exampleText - the text to show with the background colour.
		 * @param description - the description of what the given colour signifies.
		 */
		private void initializeUI(Color bgColour, String exampleText, String description)
		{
			this.setLayout(new FlowLayout(FlowLayout.LEFT));
			
			JLabel example = new JLabel(exampleText);
			example.setOpaque(true);
			example.setBackground(bgColour);
			
			JLabel descriptionLabel = new JLabel(description);
			
			this.add(Box.createHorizontalStrut(PADDING));
			this.add(example);
			this.add(descriptionLabel);
		}
		
		/**
		 * Serial version for DescriptionPanel.
		 */
		private static final long serialVersionUID = -8530297776468395485L;
	}
	
	/**
	 * Serial version for the Legend.
	 */
	private static final long serialVersionUID = 2915665673936892327L;
}
