package haplotyping.views.uicomponents;

import haplotyping.views.uicomponents.misc.ErrorMessageColour;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class to display the error message panel.
 */
public class MessagePanel extends JPanel
{
	private String initialMessage;
	private JLabel messageDisplayer;
	
	/**
	 * Constructor for the MessagePanel that has an initial message.
	 * Note: passing null initialMessage makes blank display.
	 * @pre true;
	 * @post this.isVisible() == true;
	 * @param initialMessage - the initial message to display.
	 */
	public MessagePanel(String initialMessage)
	{
		super();
		this.initializeUI(initialMessage);
	}
	
	/**
	 * Helper method to initialize the components for the MessagePanel.
	 * @pre true;
	 * @post true;
	 * @param initialMessage - the initial message to display.
	 */
	private void initializeUI(String initialMessage)
	{
		this.initialMessage = initialMessage;
		if(initialMessage == null)
		{
			this.messageDisplayer = new JLabel();
		}
		else
		{
			this.messageDisplayer = new JLabel(initialMessage);
		}
		
		this.add(messageDisplayer);
	}
	
	/**
	 * Display the initial message that was passed into the constructor.
	 * @pre true;
	 * @post true;
	 */
	public void displayInitialMessage()
	{
		if(initialMessage == null)
		{
			this.messageDisplayer.setText("");
		}
		else
		{
			this.messageDisplayer.setText(this.initialMessage);
		}
		
		this.messageDisplayer.setForeground(Color.BLACK);
	}

	/**
	 * Display the given message as a error message.
	 * @pre message != null;
	 * @post true;
	 * @param message - the message to display.
	 */
	public void displayErrorMessage(String message)
	{
		this.messageDisplayer.setText(message);
		this.messageDisplayer.setForeground(new ErrorMessageColour());
	}

	/**
	 * Display the given message as a regular message.
	 * @pre message != null;
	 * @post true;
	 * @param message - the message to display.
	 */
	public void displayMessage(String message)
	{
		this.messageDisplayer.setText(message);
		this.messageDisplayer.setForeground(Color.BLACK);
	}
	
	/**
	 * Serial version of the MessagePanel.
	 */
	private static final long serialVersionUID = -4326532520823879709L;
}
