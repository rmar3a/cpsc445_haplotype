package haplotyping.views.uicomponents;

import haplotyping.views.uicomponents.FileChooser.Filter;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Inner class for displaying the button to select a VCF and to
 * display the VCF file selected.
 */
public class FileSelector extends JPanel
{
	private final static int INPUT_WIDTH = 250;
	private final static int INPUT_HEIGHT = 30;
	
	private JTextField fileSelected;
	private JButton selectFileBtn;
	
	/**
	 * Emtpy constructor for the FileSelector.
	 * @pre true;
	 * @post this.isVisible() == true;
	 */
	public FileSelector()
	{
		super();
		this.initializeUI();
	}
	
	/**
	 * Constructor for displaying a given file path.
	 * @pre true;
	 * @post this.isVisible() == true;
	 * @param pathToDisplay - the file path to display.
	 */
	public FileSelector(String pathToDisplay)
	{
		super();
		this.initializeUI(pathToDisplay);
	}
	
	/**
	 * Helper method to initialize the UI with the given file path.
	 * @pre true;
	 * @post this.fileSelected.getText().equals(pathToDisplay);
	 * @param pathToDisplay - the file path to display.
	 */
	public void initializeUI(String pathToDisplay)
	{
		this.initializeUI();
		this.fileSelected.setText(pathToDisplay);
	}
	
	/**
	 * Helper method to initialize the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.fileSelected = new JTextField();
		this.fileSelected.setPreferredSize(new Dimension(INPUT_WIDTH, INPUT_HEIGHT));
		this.fileSelected.setMaximumSize(new Dimension(INPUT_WIDTH, INPUT_HEIGHT));
		this.selectFileBtn = new JButton("Select");
		this.initializeSelectBtn();
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		this.add(this.fileSelected);
		this.add(this.selectFileBtn);
	}
	
	/**
	 * Helper method add ActionListener to the select button.
	 * @pre true;
	 * @post true;
	 */
	private void initializeSelectBtn()
	{
		this.selectFileBtn.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				FileChooser fileChooser = new FileChooser("Select VCF File", null, Filter.VCF);
				File choosenFile = fileChooser.getSelectedFile();
				
				// if file selected display the file path
				if(fileChooser.getButtonNumber() == JFileChooser.APPROVE_OPTION)
				{
					setFileDisplayed(choosenFile.getAbsolutePath());
				}
			}
		});
	}
	
	/**
	 * Display the given file path.
	 * @pre fileLocation != null;
	 * @post true;
	 * @param fileLocation - the location of the selected file.
	 */
	public void setFileDisplayed(String fileLocation)
	{
		this.fileSelected.setText("");
		this.fileSelected.setText(fileLocation);
	}
	
	/**
	 * Retrieve the text that is displayed.  The text should be the path to the file
	 * that was selected with white space trimmed.
	 * @pre true;
	 * @post true;
	 * @return the String that is the path to the selected file.
	 */
	public String getFilePathDisplayed()
	{
		return this.fileSelected.getText().trim();
	}
			
	/**
	 * Serial version for FileSelector.
	 */
	private static final long serialVersionUID = 1L;
}
