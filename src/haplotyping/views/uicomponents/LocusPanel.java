package haplotyping.views.uicomponents;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome.Locus;
import haplotyping.views.uicomponents.misc.AlleleFont;
import haplotyping.views.uicomponents.misc.HighlightColour;
import haplotyping.views.uicomponents.misc.MissingDataColour;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Class for displaying the locus information for an individual.
 */
public class LocusPanel extends JPanel
{
	/**
	 * Constructor for LocusPanel.
	 * @pre locus != null && alleles != null;
	 * @post true;
	 * @param locus - the Locus containing the locus position information.
	 * @param alleles - the alleles found at the given locus.
	 * @param maxAlleleSize - the length of the longest allele.
	 */
	public LocusPanel(Locus locus, AlleleInfo alleles, int maxAlleleSize)
	{
		this.initializeUI(locus, alleles, maxAlleleSize);
	}
	
	/**
	 * Helper method to populate the panel.
	 * @pre locus != null;
	 * @post true;
	 * @param locus - the Locus containing the locus position information.
	 * @param alleles - the alleles found at the given locus.
	 * @param maxAlleleSize - the length of the longest allele.
	 */
	private void initializeUI(Locus locus, AlleleInfo alleleInfo, int maxAlleleSize)
	{
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
		this.setToolTipText("<html> Chromosome: " + locus.getChromosome() + "<br>Position: " + String.valueOf(locus.getPosition()));
        
        if(alleleInfo != null && alleleInfo.getUnphasedAlleles().size() != 0)
        {
                ArrayList<String> alleles = alleleInfo.getUnphasedAlleles();
                
				this.addAllele(alleles.get(0), maxAlleleSize);
                
                if(alleleInfo.isHomozygous())
                {
                	this.addAllele(alleles.get(0), maxAlleleSize);
                }
                else
                {
                        if(alleles.size() < 2)
                        {
                                this.addMissingAllele(maxAlleleSize);
                        }
                        else
                        {
                        		this.addAllele(alleles.get(1), maxAlleleSize);
                        }
                }
        }
        else
        {
                this.addMissingAllele(maxAlleleSize);
                this.addMissingAllele(maxAlleleSize);
        }
        
        this.initializeMouseOver();
	}
	
	/**
	 * Helper method to initialize the highlight effect when mousing over.
	 * @pre true;
	 * @post true;
	 */
	private void initializeMouseOver()
	{
		this.setBorder(new LineBorder(getBackground(), 3));
		
		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseExited(MouseEvent arg0)
			{
				setBorder(new LineBorder(getBackground(), 3));
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0)
			{
				setBorder(new LineBorder(new HighlightColour(), 3));
			}
		});
	}
	
	/**
	 * Helper method to add a allele to the locus panel; if allele shorter
	 * than longest allele at the locus, "-" are added to pad the space.
	 * @pre allele != null && maxAlleleSize >= 0;
	 * @post true;
	 * @param allele - the sequence of the allele.
	 * @param maxAlleleSize - the length of the longest allele at the locus.
	 */
	private void addAllele(String allele, int maxAlleleSize)
	{	
		String missingDataText = "<font style=\"background-color:#FF3B72;\">";			
		
		if(allele.length() < maxAlleleSize)
		{	
			// for loop to create missing data text
			for(int i = 0; i < maxAlleleSize - allele.length(); i++)
			{
				missingDataText += "-";
			}	

			missingDataText += "</font></html>";
			JLabel alleleLabel = new JLabel("<html>" + allele + missingDataText);
			alleleLabel.setFont(new AlleleFont());
			this.add(alleleLabel);
		}
		else
		{
			JLabel alleleLabel = new JLabel(allele);
			alleleLabel.setFont(new AlleleFont());
			this.add(alleleLabel);
		}
	}
	
	/**
	 * Helper method to add a missing allele marker to the display.
	 * @pre true;
	 * @post true;
	 * @param maxAlleleSize - the size of the longest allele at the locus.
	 */
	private void addMissingAllele(int maxAlleleSize)
	{
		String missingDataText = "";
		// for loop to create missing data text
		for(int i = 0; i < maxAlleleSize; i++)
		{
			missingDataText += "-";
		}
		
		JLabel missingData = new JLabel(missingDataText);
		missingData.setFont(new AlleleFont());
		missingData.setOpaque(true);
		missingData.setBackground(new MissingDataColour());
		this.add(missingData);
	}
	
	/**
	 * Serial version for LocusPanel.
	 */
	private static final long serialVersionUID = 7451016179889719212L;
}
