package haplotyping.views.uicomponents;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Class for filtering files based on the given extension.
 */
public class CustomFileFilter extends FileFilter
{
	private final String fileExtension;
	
	/**
	 * Constructor for the CustomFileFilter.
	 * @pre fileExtension != null;
	 * @post true;
	 * @param fileExtension - the type of file extension to filter by.  Should contain
	 * 						  the period. e.g. ".vcf" without the quotes.
	 */
	public CustomFileFilter(String fileExtension)
	{
		this.fileExtension = fileExtension.trim();
	}
	
	/**
	 * Defining acceptable file types.
	 * @pre pathGiven != null;
	 * @post true;
	 * @param pathGiven - the path of the file to be checked.
	 */
	@Override
	public boolean accept(File pathGiven)
	{
		if(pathGiven.isDirectory())
		{
			return true;
		}
		
		if(pathGiven.getName().toLowerCase().endsWith(this.fileExtension))
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Description that will be placed in the filechooser's filter dropdown.
	 * @pre true;
	 * @post true;
	 * @return the String to display in the filechooser's filter dropdown.
	 */
	@Override
	public String getDescription()
	{
		return "*" + this.fileExtension + " Only";
	}	
}
