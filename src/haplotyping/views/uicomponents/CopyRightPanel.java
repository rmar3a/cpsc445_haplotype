package haplotyping.views.uicomponents;

import java.awt.ComponentOrientation;

import haplotyping.views.uicomponents.misc.CopyRightColour;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class to display the Copy Right message at the bottom of the program.
 */
public class CopyRightPanel extends JPanel
{
	/**
	 * Constructor for the CopyRightPanel.
	 * @pre true;
	 * @post true;
	 */
	public CopyRightPanel()
	{
		super();
		this.initializeUI();
	}
	
	/**
	 * Helper method to initialize the CopyRightPanel.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		this.add(new CopyRightLabel());
	}
	
	/**
	 * Inner class for displaying the Copy Right text at bottom of the UI.
	 */
	public class CopyRightLabel extends JLabel
	{
		private final static String COPY_RIGHT_TEXT = "HaploFinder Copyright 2013.  All Rights Reserved.";
		
		/**
		 * Constructor for the CopyRightLabel.
		 * @pre true;
		 * @post true;
		 */
		public CopyRightLabel()
		{
			super(COPY_RIGHT_TEXT);
			this.initializeUI();
		}
		
		/**
		 * Helper method to initialize the custom label.
		 * @pre true;
		 * @post true;
		 */
		private void initializeUI()
		{
			this.setForeground(new CopyRightColour());
		}
		
		/**
		 * Serial version for the CopyRightLabel.
		 */
		private static final long serialVersionUID = 6103281616325828232L;
	}
	
	/**
	 * Serial version for the CopyRightPanel.
	 */
	private static final long serialVersionUID = -7243860748307052901L;	
}
