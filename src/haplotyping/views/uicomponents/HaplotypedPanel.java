package haplotyping.views.uicomponents;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.VCFInfo;
import haplotyping.models.VCFInfo.Relation;
import haplotyping.views.uicomponents.misc.AlleleFont;
import haplotyping.views.uicomponents.misc.DadAlleleColour;
import haplotyping.views.uicomponents.misc.HighlightColour;
import haplotyping.views.uicomponents.misc.MissingDataColour;
import haplotyping.views.uicomponents.misc.MomAlleleColour;
import haplotyping.views.uicomponents.misc.UndeterminedColour;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Class to display a VCFInfo object that has been haplotyped.
 */
public class HaplotypedPanel extends JPanel
{
	private JPanel individualInfo;
	
	/**
	 * Constructor for the HaplotypedPanel.
	 * @pre vcfFile != null;
	 * @post true;
	 * @param vcfFile - the VCF file information for the haplotyped individual.
	 * @param genome - the genome for the given VCF file.
	 */
	public HaplotypedPanel(VCFInfo vcfFile, Genome genome)
	{
		super();
		this.initializeUI(vcfFile, genome);
	}
	
	/**
	 * Helper method to display the information of the given VCF file.
	 * @pre file != null;
	 * @post true;
	 * @param file - the VCFInfo to display.
	 * @param genome - the genome for the given VCF file.
	 */
	private void initializeUI(VCFInfo file, Genome genome)
	{
		this.individualInfo = new JPanel();
		this.individualInfo.setLayout(new BoxLayout(this.individualInfo, BoxLayout.Y_AXIS));
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setAlignmentX(RIGHT_ALIGNMENT);
		this.individualInfo.add(new JLabel(" Relation: " + file.getRelation()));

		String individualID = file.getIndividualID();
		
		if(individualID.contains("/"))
		{
			String[] values = individualID.split("/");
			individualID = values[values.length - 1];
		}
		
		this.individualInfo.add(new JLabel(" Ind ID: " + individualID));
		this.add(this.individualInfo);
		this.add(Box.createHorizontalStrut(10));
		
		if(file.getRelation().equals(Relation.CHILD))
		{
			for(Locus locus : genome.getSortedLoci())
			{
				AlleleInfo alleleInfo = file.getLocusAlleleInfo(locus);
				int maxAlleleSize = genome.getMaxAlleleSize(locus);
				this.add(new PhasedLocusPanel(locus, alleleInfo, maxAlleleSize));
			}
		}
		else
		{
			for(Locus locus : genome.getSortedLoci())
			{
				AlleleInfo alleleInfo = file.getLocusAlleleInfo(locus);
				int maxAlleleSize = genome.getMaxAlleleSize(locus);
				this.add(new LocusPanel(locus, alleleInfo, maxAlleleSize));
			}
		}
	}
	
	/**
	 * Inner class for displaying the phased locus information for an individual.
	 */
	public class PhasedLocusPanel extends JPanel
	{
		/**
		 * Constructor for PhasedLocusPanel.
		 * @pre locus != null && alleleInfo != null;
		 * @post true;
		 * @param locus - the Locus containing the locus position information.
		 * @param alleles - the alleles found at the given locus.
		 * @param maxAlleleSize - the length of the longest allele.
		 */
		public PhasedLocusPanel(Locus locus, AlleleInfo alleleInfo, int maxAlleleSize)
		{
			this.initializeUI(locus, alleleInfo, maxAlleleSize);
		}
		
		/**
		 * Helper method to populate the panel.
		 * @pre locus != null;
		 * @post true;
		 * @param locus - the Locus containing the locus position information.
		 * @param alleleInfo - the allele information for a given locus.
		 * @param maxAlleleSize - the length of the longest allele.
		 */
		private void initializeUI(Locus locus, AlleleInfo alleleInfo, int maxAlleleSize)
		{
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			
			this.setToolTipText("<html> Chromosome: " + locus.getChromosome() + "<br>Position: " + String.valueOf(locus.getPosition()));
			
			if(alleleInfo != null)
			{
                if(alleleInfo.isHomozygous())
                {
                	String allele = alleleInfo.getUnphasedAlleles().get(0);
                	this.addDadAllele(allele, maxAlleleSize);
                	this.addMomAllele(allele, maxAlleleSize);
                }
                else
                {
					if(alleleInfo.getDadAllele() == null)
					{
						this.addUndeterminedAllele(maxAlleleSize);
					}
					else
					{
						this.addDadAllele(alleleInfo.getDadAllele(), maxAlleleSize);					
					}
					
					if(alleleInfo.getMomAllele() == null)
					{
						this.addUndeterminedAllele(maxAlleleSize);
					}
					else
					{
						this.addMomAllele(alleleInfo.getMomAllele(), maxAlleleSize);
					}
                }
			}
			else
			{
				this.addMissingAllele(maxAlleleSize);
				this.addMissingAllele(maxAlleleSize);
			}
			
            this.initializeMouseOver();
		}
		
		/**
		 * Helper method to initialize the highlight effect when mousing over.
		 * @pre true;
		 * @post true;
		 */
		private void initializeMouseOver()
		{
			this.setBorder(new LineBorder(getBackground(), 3));
			
			this.addMouseListener(new MouseAdapter()
			{
				@Override
				public void mouseExited(MouseEvent arg0)
				{
					setBorder(new LineBorder(getBackground(), 3));
				}
				
				@Override
				public void mouseEntered(MouseEvent arg0)
				{
					setBorder(new LineBorder(new HighlightColour(), 3));
				}
			});
		}
		
		/**
		 * Helper method to add a missing allele marker to the display.
		 * @pre true;
		 * @post true;
		 * @param maxAlleleSize - the length of the longest allele at the locus.
		 */
		private void addMissingAllele(int maxAlleleSize)
		{
			String missingDataText = "";
			// for loop to create missing data text
			for(int i = 0; i < maxAlleleSize; i++)
			{
				missingDataText += "-";
			}
			
			JLabel missingData = new JLabel(missingDataText);
			missingData.setFont(new AlleleFont());
			missingData.setOpaque(true);
			missingData.setBackground(new MissingDataColour());
			this.add(missingData);
		}
		
		/**
		 * Helper method to add a missing allele marker to the display.
		 * @pre true;
		 * @post true;
		 * @param maxAlleleSize - the length of the longest allele at the locus.
		 */
		private void addUndeterminedAllele(int maxAlleleSize)
		{
			String missingDataText = "";
			// for loop to create missing data text
			for(int i = 0; i < maxAlleleSize; i++)
			{
				missingDataText += "?";
			}
			
			JLabel missingData = new JLabel(missingDataText);
			missingData.setFont(new AlleleFont());
			missingData.setOpaque(true);
			missingData.setBackground(new UndeterminedColour());
			this.add(missingData);
		}
		
		/**
		 * Helper method to add an allele marked as mom haplotype.
		 * @pre allele != null;
		 * @post true;
		 * @param allele - the allele to display, marked as mom haplotype.
		 * @param maxAlleleSize - the length of the longest allele at the locus.
		 */
		private void addMomAllele(String allele, int maxAlleleSize)
		{
			String missingDataText = "<font style=\"background-color:#FF3B72;\">";			
			
			if(allele.length() < maxAlleleSize)
			{	
				// for loop to create missing data text
				for(int i = 0; i < maxAlleleSize - allele.length(); i++)
				{
					missingDataText += "-";
				}	

				missingDataText += "</font></html>";
				JLabel alleleLabel = new JLabel("<html>" + allele + missingDataText);
				alleleLabel.setFont(new AlleleFont());
				alleleLabel.setOpaque(true);
				alleleLabel.setBackground(new MomAlleleColour());
				this.add(alleleLabel);
			}
			else
			{
				JLabel alleleLabel = new JLabel(allele);
				alleleLabel.setFont(new AlleleFont());
				alleleLabel.setOpaque(true);
				alleleLabel.setBackground(new MomAlleleColour());
				this.add(alleleLabel);
			}
		}
		
		/**
		 * Helper method to add a missing allele marker to the display.
		 * @pre allele != null;
		 * @post true;
		 * @param allele - the allele to display, marked as dad haplotype.
		 * @param maxAlleleSize - the length of the longest allele at the locus.
		 */
		private void addDadAllele(String allele, int maxAlleleSize)
		{
			String missingDataText = "<font style=\"background-color:#FF3B72;\">";			
			
			if(allele.length() < maxAlleleSize)
			{	
				// for loop to create missing data text
				for(int i = 0; i < maxAlleleSize - allele.length(); i++)
				{
					missingDataText += "-";
				}	

				missingDataText += "</font></html>";
				JLabel alleleLabel = new JLabel("<html>" + allele + missingDataText);
				alleleLabel.setFont(new AlleleFont());
				alleleLabel.setOpaque(true);
				alleleLabel.setBackground(new DadAlleleColour());
				this.add(alleleLabel);
			}
			else
			{
				JLabel alleleLabel = new JLabel(allele);
				alleleLabel.setFont(new AlleleFont());
				alleleLabel.setOpaque(true);
				alleleLabel.setBackground(new DadAlleleColour());
				this.add(alleleLabel);
			}
		}
		
		/**
		 * Serial version of PhasedLocusPanel.
		 */
		private static final long serialVersionUID = 5200088879302813637L;
	}
	
	/**
	 * Serial version of HaplotypedPanel.
	 */
	private static final long serialVersionUID = -8002069240089706492L;
}

