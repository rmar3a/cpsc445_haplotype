package haplotyping.views.uicomponents;

import java.awt.ComponentOrientation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * Menu bar for the HaploFinder program.
 */
public class HaploFinderMenuBar extends JMenuBar
{
	private static int PADDING = 20;
	private JMenu help;
	private JMenuItem legendMenu;
	private JMenuItem fileFormat;
	
	/**
	 * Constructor for the HaploFinderMenuBar.
	 * @pre true;
	 * @post true;
	 */
	public HaploFinderMenuBar()
	{
		super();
		this.initializeUI();
	}
	
	/**
	 * Helper method to generate the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		KeyStroke ctrlL = KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK);
		
		// initialize JMenu values
		this.help = new JMenu("Help");
		
		// initialize JMenuItem values
		this.legendMenu = new JMenuItem("Legend");
		this.legendMenu.setAccelerator(ctrlL);
		this.fileFormat = new JMenuItem("Bulk Import Info");
		
		this.initializeListeners();
		
		// add components together
		this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		this.help.add(this.legendMenu);
		this.help.add(this.fileFormat);
		this.add(Box.createHorizontalStrut(PADDING));
		this.add(this.help);
	}
	
	/**
	 * Helper method to initialize the listeners for the menu items.
	 * @pre true;
	 * @post true;
	 */
	private void initializeListeners()
	{
		this.legendMenu.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Legend legend = new Legend();
				legend.setLocationRelativeTo(null);
				legend.setAlwaysOnTop(true);
				legend.addWindowListener(new WindowAdapter()
				{
					@Override
					public void windowClosed(WindowEvent e)
					{
						legendMenu.setEnabled(true);
					}
				});
				
				legendMenu.setEnabled(false);
			}
		});
		
		this.fileFormat.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				BulkImportInfo bulkImportInfo = new BulkImportInfo();
				bulkImportInfo.setLocationRelativeTo(null);
				bulkImportInfo.setAlwaysOnTop(true);
				bulkImportInfo.addWindowListener(new WindowAdapter()
				{
					@Override
					public void windowClosed(WindowEvent e)
					{
						fileFormat.setEnabled(true);
					}
				});
				
				fileFormat.setEnabled(false);
			}
		});
	}

	/**
	 * Serial version for the HaploFinderMenuBar.
	 */
	private static final long serialVersionUID = -929352076998796962L;
}
