package haplotyping.views.uicomponents;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Class to display the rows of selected VCF files.
 */
public class FamilySelector extends JPanel
{
	// enum for selecting between the UI objects for moms and children
	public enum Relation {CHILD, MOM}
	
	private JPanel displayHolder;
	private JPanel momHolder;
	private JPanel childHolder;
	
	/**
	 * Constructor for the FamilySelector.
	 * @pre true;
	 * @post true;
	 */
	public FamilySelector()
	{
		super();
		this.initializeUI();
	}
	
	/**
	 * Helper method to initialize the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.displayHolder = new JPanel();
		this.momHolder = new JPanel();
		this.childHolder = new JPanel();
		
		// setting layout
		this.displayHolder.setLayout(new BoxLayout(this.displayHolder, BoxLayout.X_AXIS));
		this.momHolder.setLayout(new BoxLayout(this.momHolder, BoxLayout.Y_AXIS));
		this.childHolder.setLayout(new BoxLayout(this.childHolder, BoxLayout.Y_AXIS));
		
		// setting alignment to top
		this.momHolder.setAlignmentY(Component.TOP_ALIGNMENT);
		this.childHolder.setAlignmentY(Component.TOP_ALIGNMENT);
		
		// adding components to other components
		this.momHolder.add(new FileSelector());
		this.childHolder.add(new FileSelector());
		this.displayHolder.add(this.momHolder);
		this.displayHolder.add(Box.createHorizontalStrut(10));
		this.displayHolder.add(this.childHolder);
		this.add(this.displayHolder);
	}
	
	/**
	 * Add the file paths for the list of given VCF files.  Will clear the
	 * file path display first if no file paths have been added yet.
	 * @pre filePaths != null && memberType != null;
	 * @post true;
	 * @param filePaths - the list of file paths to display.
	 * @param memberType - Relation.CHILD if children VCF file paths, Relation.MOM
	 * 					   if mom VCF file paths.
	 */
	public void loadFilePaths(ArrayList<String> filePaths, Relation memberType)
	{
		JPanel holder = this.getHolder(memberType);
		
		if(this.isCleanImport(holder))
		{
			holder.removeAll();
		}
		
		for(String path : filePaths)
		{
			holder.add(new FileSelector(path));
		}
		
		this.revalidate();
	}
	
	/**
	 * Helper method to determine if any files have been selected by iterating through
	 * the given JPanel and searching for any FileSelectors with text in them.
	 * @pre true;
	 * @post true;
	 * @param holder - the JPanel to search for FileSelectors with any values.
	 * @return false if file paths are displayed; true otherwise.
	 */
	private boolean isCleanImport(JPanel holder)
	{
		Component[] components = holder.getComponents();
		
		for(Component displayUnit : components)
		{
			if(displayUnit instanceof FileSelector)
			{
				if(!((FileSelector) displayUnit).getFilePathDisplayed().equals(""))
				{
					return false;
				}
			}
		}
			
		return true; 
	}
	
	/**
	 * Add a new mother file selector.
	 * @pre true;
	 * @post true;
	 */
	public void addMother()
	{
		this.momHolder.add(new FileSelector());
		this.revalidate();
	}
	
	/**
	 * Add a new child file selector.
	 * @pre true;
	 * @post true;
	 */
	public void addChild()
	{
		this.childHolder.add(new FileSelector());
		this.revalidate();
	}
	
	/**
	 * Remove all the file selectors for the mother selectors and go back to default
	 * view.
	 * @pre true;
	 * @post this.momHolder.getComponentCount() == 1;
	 */
	public void resetMother()
	{
		this.momHolder.removeAll();
		this.momHolder.add(new FileSelector());
		this.revalidate();
	}
	
	/**
	 * Remove all the file selectors for the mother selectors and go back to default
	 * view.
	 * @pre true;
	 * @post this.childHolder.getComponentCount() == 1;
	 */
	public void resetChild()
	{
		this.childHolder.removeAll();
		this.childHolder.add(new FileSelector());
		this.revalidate();
	}
	
	/**
	 * Retrieve the ArrayList containing the file paths for the VCF files for the
	 * moms or children.
	 * @pre familyMemberType != null;
	 * @post true;
	 * @param familyMemberType - Use Relation.CHILD if want to retrieve children VCF
	 * 							 files and Relation.MOM if want to retrieve mom VCF
	 * 							 files.
	 * @return the ArrayList containing the file paths for the VCF files for the moms
	 * 		   or children.
	 */
	public ArrayList<String> getFilePaths(Relation familyMemberType)
	{
		JPanel holder = this.getHolder(familyMemberType);
		
		return this.getPaths(holder);
	}
	
	/**
	 * Helper method to iterate through the given panel and search for the FileSelector
	 * child components that contain the file paths selected.
	 * @pre panel != null;
	 * @post true;
	 * @param panel - the JPanel containing FileSelector panels with the files selected.
	 * @return ArrayList containing the file paths selected.
	 */
	private ArrayList<String> getPaths(JPanel panel)
	{
		ArrayList<String> files = new ArrayList<String>();
		Component[] components = panel.getComponents();
		
		// for loop to find all the FileSelector components and extract the files
		// paths selected
		for(Component component : components)
		{
			if(component instanceof FileSelector)
			{
				String path = ((FileSelector) component).getFilePathDisplayed();
				
				if(!path.equals(""))
				{
					files.add(path);
				}
			}
		}
		
		return files;
	}
	
	/**
	 * Helper method to determine which JPanel contains the FileSelectors for the
	 * associated type of family member.
	 * @pre memberType != null;
	 * @post true;
	 * @param memberType - Relation.CHILD if searching for the JPanel containing the
	 * 					   FileSelectors for the children, Relation.MOM if searching
	 * 					   for the JPanel containing the FileSelectors for the moms.
	 * @return the JPanel containing the FileSelectors for the family member of interest.
	 */
	private JPanel getHolder(Relation memberType)
	{	
		if(memberType.equals(Relation.CHILD))
		{
			return this.childHolder;
		}
		else
		{
			return this.momHolder;
		}
	}
	
	/**
	 * Serial version for FamilySelector.
	 */
	private static final long serialVersionUID = -8032433235096935549L;
}
