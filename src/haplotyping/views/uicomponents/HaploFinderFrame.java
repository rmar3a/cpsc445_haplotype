package haplotyping.views.uicomponents;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 * Display custom JFrame with logo.
 */
public class HaploFinderFrame extends JFrame
{
	private static final String PROJECT_DIRECTORY_PATH = System.getProperty("user.dir");
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static final String FOLDER_NAME = "DataFiles";
	private static final String ICON_FILENAME = "logoH";
	private File frameIcon;
	
	/**
	 * Constructor for the HaploFinderFrame.
	 * @pre title != null;
	 * @post this.isVisible() == true;
	 * @param title - the title that will appear on the HaploFinderFrame.
	 */
	public HaploFinderFrame(String title)
	{
		super(title);
		frameIcon = new File(PROJECT_DIRECTORY_PATH + FILE_SEPARATOR + FOLDER_NAME + FILE_SEPARATOR +
							 ICON_FILENAME + ".JPG");

		BufferedImage iconImage = null;

		try
		{
			iconImage = ImageIO.read(frameIcon);
		}
		catch (IOException e)
		{
			// Do nothing
		}
		
		this.setIconImage(iconImage);
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Serial version for the HaploFinderFrame.
	 */
	private static final long serialVersionUID = 2461245561036802670L;
}
