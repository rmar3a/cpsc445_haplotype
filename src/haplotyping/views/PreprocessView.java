package haplotyping.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import haplotyping.models.InfoBag;
import haplotyping.models.VCFInfo;
import haplotyping.presenters.PreprocessPresenter.PreprocessDisplay;
import haplotyping.views.uicomponents.PreprocessedPanel;

/**
 * Display the VCFInfo objects generated after preprocessing of the information is
 * complete.
 */
public class PreprocessView extends JPanel implements PreprocessDisplay
{
	private static final int DISPLAY_WIDTH = 760;
	private static final int DISPLAY_HEIGHT = 500;
	private JButton restartBtn;
	private JButton haplotypeBtn;
	private JPanel individualsHolder;
	
	/**
	 * Constructor for the PreprocessView.
	 * @pre momFiles != null && childFiles != null && dadFile != null;
	 * @post true;
	 * @param momFiles - the mom VCFInfo objects to display. 
	 * @param childFiles - the child VCFInfo objects to display.
	 * @param dadFile - the dad VCFInfo object to display.
	 * @param genome - the genome for the given VCF files.
	 */
	public PreprocessView()
	{
		this.initializeUI();
	}

	/**
	 * Helper method to initialize the UI.
	 * @pre true;
	 * @post true;
	 */
	private void initializeUI()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		individualsHolder = new JPanel();
		individualsHolder.setLayout(new BoxLayout(individualsHolder, BoxLayout.Y_AXIS)); 
		JPanel btnHolder = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JScrollPane scrollView = new JScrollPane(individualsHolder);
		scrollView.setPreferredSize(new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT));
		
		this.addHierarchyBoundsListener(new HierarchyBoundsAdapter()
		{	
			@Override
			public void ancestorResized(HierarchyEvent arg0)
			{
				setPreferredSize(getParent().getSize());
			}
		});
		
		this.restartBtn = new JButton("Restart");
		this.haplotypeBtn = new JButton("Haplotype");
		
		btnHolder.add(this.restartBtn);
		btnHolder.add(this.haplotypeBtn);
		
		this.add(scrollView);
		this.add(btnHolder);
	}
	
	/**
	 * Helper method to remove all VCFInfo objects currently displayed and then 
	 * display the given VCFInfo objects.
	 * @pre momFiles != null && childFiles != null && dadFile != null;
	 * @post true;
	 * @param info - the InfoBag containing the preprocessed information to display;
	 * 				 contains the moms, children, and dad VCF file information as
	 * 				 well as the genome for the given individuals.
	 */
	@Override
	public void displayPreprocessedData(InfoBag info)
	{
		this.individualsHolder.removeAll();
		this.individualsHolder.add(new PreprocessedPanel(info.dadFile, info.genome));		
		this.individualsHolder.add(new PreprocessedPanel(info.momFile, info.genome));
		
		for(VCFInfo file : info.childFiles)
		{
			this.individualsHolder.add(new PreprocessedPanel(file, info.genome));
		}
	}
	
	@Override
	public AbstractButton getRestartBtn()
	{
		return this.restartBtn;
	}
	
	@Override
	public AbstractButton getHaplotypeBtn()
	{
		return this.haplotypeBtn;
	}
	
	@Override
	public Component asComponent()
	{
		return this;
	}
	
	/**
	 * Serial version for the PreprocessView.
	 */
	private static final long serialVersionUID = -2090676012436244793L;
}
