package haplotyping.test;

import java.io.File;
import java.util.ArrayList;

import haplotyping.backend.Preprocessor;
import haplotyping.backend.VCFParser;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.VCFInfo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestPreprocess
{
	private Genome genome;
	private ArrayList<VCFInfo >momsVCFInfo;
	private ArrayList<VCFInfo> kidsVCFInfo;
	
	public final static int DIPLOID = 2;
	

	@Before
	public void setUp() throws Exception
	{
		String momsVCFPath = "./unit_test_data/HA89.mpileup.small.vcf";
		File file  = new File(momsVCFPath);
	
		
		genome = new Genome(DIPLOID);
		momsVCFInfo = VCFParser.extractInfo(file.getAbsolutePath(), genome );
		for (VCFInfo pseudomom : momsVCFInfo)
		{
			pseudomom.printHumanFriendly();
		}
		//Mom:  Chromosome=scf7180037882725, Position=24417=[C, C]
		ArrayList<String> firstMomAlleles = momsVCFInfo.get(0).getLocusAlleles("scf7180037882725", 24417);
		Assert.assertNotNull(firstMomAlleles);
		Assert.assertArrayEquals(new String[] {"C", "C"},  firstMomAlleles.toArray(new String[2]));
		
		String kidsVCFPath = "./unit_test_data/F1s.10kids.small.vcf";
		
		//All kids:  Chromosome=scf7180037882725, Position=24417=[C, C]
		kidsVCFInfo = VCFParser.extractInfo(kidsVCFPath, genome );
		for (VCFInfo kid : kidsVCFInfo)
		{
			kid.printHumanFriendly();
		}
		ArrayList<String> firstKidAlleles = kidsVCFInfo.get(0).getLocusAlleles("scf7180037882725", 24417);
		Assert.assertNotNull(firstKidAlleles);
		Assert.assertArrayEquals(new String[] {"C", "C"},  firstKidAlleles.toArray(new String[2]));
		
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void preprocess() throws Exception
	{
	
		try
		{
			boolean isDadImputeCorrect = false;
			ArrayList<String> dadAlleles;
			
			Preprocessor proc = new Preprocessor(genome);
			VCFInfo dad = new VCFInfo();
			VCFInfo mom = new VCFInfo();
			proc.imputeParentGenotype(this.momsVCFInfo, dad, mom, this.kidsVCFInfo);
			
			// Test dad homozygous.  Dad share same alleles as Mom.
			// Mom is C/C
			// All kids are C/C
			// Expect Dad C/C
			dadAlleles= dad.getLocusAlleles("scf7180037882725", 24417);
			Assert.assertNotNull(dadAlleles);
			isDadImputeCorrect = !"C".equalsIgnoreCase(dadAlleles.get(0)) || !"C".equalsIgnoreCase(dadAlleles.get(1));
			Assert.assertFalse("Locus:  scf7180037882725, pos 24417:  Expected Dad Genotype=C/C, but got " + 
					VCFInfo.getAllelesAsString(dadAlleles),  isDadImputeCorrect);
			
			// Test dad homozygous.  Dad share same alleles as Mom.
			// Mom is G/G
			// All kids are G/G
			// Expect Dad G/G
			dadAlleles = dad.getLocusAlleles("scf7180037882725", 24760);
			Assert.assertNotNull(dadAlleles);
			isDadImputeCorrect = !"G".equalsIgnoreCase(dadAlleles.get(0)) ||  !"G".equalsIgnoreCase(dadAlleles.get(1));
			Assert.assertFalse("Locus:  scf7180037882725, pos 24760:  Expected Dad Genotype=G/G, but got " + 
					VCFInfo.getAllelesAsString(dadAlleles),  isDadImputeCorrect);	
			
			// Test dad heterozygous.  Dad shares an allele with mom.
			// Mom is GAAAAAAAAAAAAA/GAAAAAAAAAAAAA  (It's hard to tell, but Mom has an inserted A)
			// Some kids are GAAAAAAAAAAAA/GAAAAAAAAAAAAA  (i.e.  heterozygous for insertion)
			// Some kids are GAAAAAAAAAAAAA/GAAAAAAAAAAAAA  (i.e.  homozygous for insertion)
			// Expect Dad GAAAAAAAAAAAA/GAAAAAAAAAAAAA    (i.e.  Dad heterozygous for insertion)
			dadAlleles = dad.getLocusAlleles("scf7180037883075", 593);
			Assert.assertNotNull(dadAlleles);
			isDadImputeCorrect = !("GAAAAAAAAAAAA".equalsIgnoreCase(dadAlleles.get(0)) &&  "GAAAAAAAAAAAAA".equalsIgnoreCase(dadAlleles.get(1))) &&
					!("GAAAAAAAAAAAAA".equalsIgnoreCase(dadAlleles.get(0)) ||  "GAAAAAAAAAAAA".equalsIgnoreCase(dadAlleles.get(1)));
			Assert.assertFalse("Locus:  scf7180037883075, pos 593:  Expected Dad Genotype=GAAAAAAAAAAAA/GAAAAAAAAAAAAA, but got " + 
					VCFInfo.getAllelesAsString(dadAlleles),  isDadImputeCorrect);
			
		}
		catch (Exception ex)
		{
			throw ex;
		}
		
	}

}
