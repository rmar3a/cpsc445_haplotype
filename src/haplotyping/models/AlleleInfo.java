package haplotyping.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import haplotyping.models.Genome.Locus;

/**
 * Represents the alleles at a single locus for a single individual.
 * 
 *
 */
public class AlleleInfo
{
	public static enum Haplotype
	{
		HAPLOTYPE_UNPHASED,  
		HAPLOTYPE_FROM_MOM_1, HAPLOTYPE_FROM_MOM_2, HAPLOTYPE_FROM_DAD_1, HAPLOTYPE_FROM_DAD_2, 
		HAPLOTYPE_GIVEN_TO_KID_1, HAPLOTYPE_GIVEN_TO_KID_2, HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS;
	}
	
	private HashMap<String, Haplotype> alleleToHaplotype;
	private boolean isHomozygous = false;
	private boolean isPhased = false;
	
	/**
	 * Constructor sets unphased alleles
	 * @pre
	 * @post
	 * @param firstAllele
	 * @param secondAllele
	 */
	public AlleleInfo(String firstAllele, String secondAllele)
	{
		this.alleleToHaplotype = new HashMap<String, AlleleInfo.Haplotype>();
		this.alleleToHaplotype.put(firstAllele, Haplotype.HAPLOTYPE_UNPHASED);
		this.alleleToHaplotype.put(secondAllele, Haplotype.HAPLOTYPE_UNPHASED);
		this.isHomozygous = firstAllele.equalsIgnoreCase(secondAllele);
		this.isPhased = false;
	}
	
	/**
	 * Constructor sets unphased alleles
	 * @pre
	 * @post
	 * @param firstAllele
	 * @param secondAllele
	 * @param isHomozygous - true if the given alleles are homozygous; false otherwise.
	 */
	public AlleleInfo(ArrayList<String> alleles)
	{
		this.alleleToHaplotype = new HashMap<String, AlleleInfo.Haplotype>();
		
		for (String allele:alleles )
		{
			this.alleleToHaplotype.put(allele, Haplotype.HAPLOTYPE_UNPHASED);
		}
		
		if(alleles.size() == 2)
		{
			this.isHomozygous = alleles.get(0).equalsIgnoreCase(alleles.get(1));
		}
		
		this.isPhased = false;
	}
	
	/**
	 * Constructor sets phased alleles
	 * @pre
	 * @post
	 * @param firstAllele
	 * @param secondAllele
	 */
	public AlleleInfo(String momAllele, Haplotype momAllelePhase, String dadAllele, Haplotype dadAllelePhase)
	{
		this.alleleToHaplotype = new HashMap<String, AlleleInfo.Haplotype>();
		this.alleleToHaplotype.put(momAllele, momAllelePhase);
		this.alleleToHaplotype.put(dadAllele, dadAllelePhase);
		this.isHomozygous = momAllele.equalsIgnoreCase(dadAllele);
		this.isPhased = true;
	}
	
	/**
	 * Sets haplotypes for alleles
	 * @pre
	 * @post
	 * @param firstAllele
	 * @param firstAlleleHaplotype
	 * @param secondAllele
	 * @param secondAlleleHaplotype
	 */
	public void setPhasing(String firstAllele, Haplotype firstAlleleHaplotype, String secondAllele, Haplotype secondAlleleHaplotype)
	{
		isPhased = true;
		alleleToHaplotype.put(firstAllele, firstAlleleHaplotype);
		alleleToHaplotype.put(secondAllele, secondAlleleHaplotype);
	}
	
	/**
	 * Determines if the given AlleleInfo is homozygous.
	 * @pre true;
	 * @post true;
	 * @return true if the AlleleInfo contains homozygous information; false otheriwse.
	 */
	public boolean isHomozygous()
	{
		return this.isHomozygous;
	}
	
	public boolean isPhased()
	{
		return this.isPhased;
	}
	
	/**
	 * Gets an array list of alleles in random order.
	 * @pre
	 * @post
	 * @return
	 */
	public ArrayList<String> getUnphasedAlleles()
	{	
		if (this.isHomozygous)
		{	
			String allele = alleleToHaplotype.keySet().iterator().next();
			ArrayList<String> alleles = new ArrayList<String>(Collections.nCopies(2, allele));
			return alleles;
		}
		else
		{	
			return new ArrayList<String>(alleleToHaplotype.keySet());
		}
		
	}
	
	public Set<String> getUnphasedAllelesSet()
	{
		return alleleToHaplotype.keySet();
	}
	
	public String getMomAllele ()
	{
		for (String allele : alleleToHaplotype.keySet())
		{
			Haplotype  haplotype = alleleToHaplotype.get(allele);
			if (haplotype == Haplotype.HAPLOTYPE_FROM_MOM_1 || 
					haplotype == Haplotype.HAPLOTYPE_FROM_MOM_2)
			{
				return allele;
			}
		}
		return null;
	}
	
	public String getDadAllele ()
	{
		for (String allele : alleleToHaplotype.keySet())
		{
			Haplotype  haplotype = alleleToHaplotype.get(allele);
			if (haplotype == Haplotype.HAPLOTYPE_FROM_DAD_1 || 
					haplotype == Haplotype.HAPLOTYPE_FROM_DAD_2)
			{
				return allele;
			}
		}
		return null;
	}
	
	/**
	 * Gets the allele that is not the given.
	 * ASSUMES diploid
	 * @pre
	 * @post
	 * @param allele
	 * @return
	 */
	public String getOtherAllele(String allele)
	{		
		Set<String> alleles = this.alleleToHaplotype.keySet();
		if (alleleToHaplotype.size() > 1)
		{
			alleles.remove(allele);
		}		
		Iterator<String> iter =  alleles.iterator();
		return iter.next();
		
	}
	/**
	 * Returns the haplotype for the allele given by this individual's dad.
	 * 
	 * @pre
	 * @post
	 * @return
	 */
	public Haplotype getHaplotype (String allele)
	{
		return alleleToHaplotype.get(allele);
	}
	
	@Override
	/**
	 * Returns true if the unphased alleles are the same
	 */
	public boolean equals(Object o1) {
		if (o1 == null)
		{
			return false;
		}
		AlleleInfo a = (AlleleInfo) o1;
		
		ArrayList<String> aUnphasedAlleles = a.getUnphasedAlleles();
        ArrayList<String> unphasedAlleles = this.getUnphasedAlleles();
        if (unphasedAlleles.size() !=  aUnphasedAlleles.size())
        {
        	return false;
        }
        for (String aAllele : aUnphasedAlleles)
        {
        	if (!alleleToHaplotype.containsKey(aAllele))
        	{
        		return false;
        	}
        }
        return true;
    }
	
	
	 @Override 
	 /** 
	  * Makes a hash out of the concatenated sorted alleles.
	  */
	 public int hashCode() {
		 Set<String> alleles = this.alleleToHaplotype.keySet();
		 ArrayList<String> list = new ArrayList<String>(alleles);
		 Collections.sort(list);
		
		 String combinedKey = StringUtils.join(list, "_"); 
		 return combinedKey.hashCode();
	 }
	 
	 @Override
	 /**
	  * allele Info as string.
	  * If phased parent:  first allele|2nd allele
	  * If phased kid:   mom allele|dad allele
	  * If unphased:  allele/another allele
	  *
	  */
	 public String toString()
	 {
		 String separator = this.isPhased ? "|" : "/";
		 Set<String> alleles = this.alleleToHaplotype.keySet();
		 	 
		 String firstAllele  = null;
		 String secondAllele = null;
		 for (String allele: alleles)
		 {
			 Haplotype haplo = alleleToHaplotype.get(allele);
			 switch (haplo)
			 {
				 case  HAPLOTYPE_GIVEN_TO_KID_1:
				 case  HAPLOTYPE_FROM_MOM_1:
				 case  HAPLOTYPE_FROM_MOM_2:
				 {
					 firstAllele = allele;
					 break;
				 }
				 case HAPLOTYPE_GIVEN_TO_KID_2:
				 case HAPLOTYPE_FROM_DAD_1:
				 case HAPLOTYPE_FROM_DAD_2:
				 {
					 secondAllele = allele;
					 break;
				 }
				 case HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS :
				 {
					 firstAllele = allele;
					 secondAllele = allele;
					 break;
				 }
				 case HAPLOTYPE_UNPHASED :
				 {					 
					 if (firstAllele == null)
					 {
						 firstAllele = allele;
					 }
					 else if (secondAllele == null)
					 {
						 secondAllele = allele;
					 }
					 break;
				 }
			 }
		 }
		 if (this.isHomozygous && secondAllele == null)
		 {
			 secondAllele = firstAllele;
		 }
		 else if (this.isHomozygous && firstAllele == null)
		 {
			 firstAllele = secondAllele;
		 }
		 if (firstAllele == null || secondAllele == null)
		 {
			 System.out.println("test breakpo;int");
		 }
		 StringBuilder str = new StringBuilder();		
		 str.append(firstAllele).append(separator).append(secondAllele);
		 
		 return str.toString();
	 }
	 
}
