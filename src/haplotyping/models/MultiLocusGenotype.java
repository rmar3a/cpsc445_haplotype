package haplotyping.models;

import haplotyping.models.AlleleInfo.Haplotype;
import haplotyping.models.Genome.Locus;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;


import org.apache.commons.lang.StringUtils;

/**
 * Represents a genotype comprised of multiple loci
 * 
 * TODO:  this repeats much of VCFInfo.  Refactor VCFInfo to use this class.
 */
public class MultiLocusGenotype
{

	private HashMap<Locus, AlleleInfo> locusToAlleleMap;
	
	/**
	 * 
	 * @pre true;
	 * @post true;
	 */
	public MultiLocusGenotype()
	{
		locusToAlleleMap  = new HashMap<Locus, AlleleInfo>();
	}
	
	
	/**
	 * Retrieve the location of the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public AlleleInfo getLocusAlleleInfo(Genome.Locus locus)
	{
		return this.locusToAlleleMap.get(locus);
	}
	
	/**
	 * Retrieve the location of the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public ArrayList<String> getLocusAlleles(Genome.Locus locus)
	{
		AlleleInfo info = this.locusToAlleleMap.get(locus);
		
		if(info != null)
		{
			return info.getUnphasedAlleles();
		}
		
		return null;
	}
	
	/**
	 * Set the location and the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public void addLocusAlleles(Genome.Locus locus, ArrayList<String> alleles)
	{
		AlleleInfo alleleInfo = new AlleleInfo(alleles);
		this.locusToAlleleMap.put(locus, alleleInfo);
	}
	

	
	public static String getAllelesAsString(ArrayList<String> alleles)
	{
		return StringUtils.join(alleles, '/');
	}
	
	/**
	 * 
	 * @pre
	 * @post
	 * @return  Set of loci with alleles
	 */
	public Set<Locus> getLociWithAlleles()
	{
		return this.locusToAlleleMap.keySet();
	}
		
	@Override
	public boolean equals(Object o) {
		MultiLocusGenotype g = (MultiLocusGenotype) o;
		Set<Locus> gLoci = g.getLociWithAlleles();
		
		if (gLoci.size() != this.locusToAlleleMap.keySet().size())
		{
			return false;
		}
		for (Locus gLocus: gLoci)
		{
			AlleleInfo gAllele = g.getLocusAlleleInfo(gLocus);
			if (!this.locusToAlleleMap.containsKey(gLocus) || !this.locusToAlleleMap.get(gLocus).equals(gAllele))
			{
				return false;
			}
		}
		return true;
    }
		
	 @Override 
	 public int hashCode() {
		 return this.locusToAlleleMap.hashCode();
	 }
	 
	 @Override 
	 public String toString() {
		 Set<Locus> loci = this.locusToAlleleMap.keySet();
		 StringBuilder str = new StringBuilder();
		 for (Locus locus: loci)
		 {
			 if (str.length() > 0)
			 {
				 str.append("; ");
			 }
			 str.append(locus).append(" : ");
			 str.append(this.locusToAlleleMap.get(locus).toString());
		 }
		 return str.toString();
	 }



}
