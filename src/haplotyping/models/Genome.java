package haplotyping.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


/**
 * Contains information about the genome,
 * and stats such as how many individuals have data at any location
 */
public class Genome
{
	private ArrayList<Locus> loci;
	private HashMap<Locus, Integer> maxAlleleSize;
	private int ploidy;
	
	public static int DIPLOID_N = 2;
	
	/**
	 * thePloidy is the total number chromosomes for each homologous set.
	 * @pre
	 * @post
	 * @param thePloidy
	 */
	public Genome(int thePloidy)
	{
		loci = new ArrayList<Locus>();
		this.maxAlleleSize = new HashMap<Genome.Locus, Integer>(17);
		ploidy = thePloidy;
	}
	
	final public static class Locus implements Comparable
	{
		private String m_chromosome;
		private int m_position;
		public int maxAlleleSize;
		
		public Locus (String chromosome, int position)
		{
			m_chromosome = chromosome;
			m_position = position;
		}	
		public String getChromosome()
		{
			return m_chromosome;
		}
		public int getPosition()
		{
			return this.m_position;
		}

		@Override
		public int compareTo(Object o1)
		{
			Locus l1 = (Locus) o1;
            int chromoCmp = this.getChromosome().compareTo(l1.getChromosome());
            if (chromoCmp == 0)
            {
            	return this.getPosition()  - l1.getPosition();
            }
            else
            {
            	return chromoCmp;
            }
        }
		
		@Override
		public boolean equals(Object o1) {
			Locus l1 = (Locus) o1;
            boolean isChromoSame = this.getChromosome().equalsIgnoreCase(l1.getChromosome());
            if (isChromoSame)
            {
            	return this.getPosition() ==  l1.getPosition();
            }
            else
            {
            	return isChromoSame;
            }
        }
		
		@Override
		public String toString() {
			return "Chromosome=" + this.m_chromosome + ", Position=" + this.m_position;
        }
		
		 @Override 
		 public int hashCode() {
			 String combinedKey = this.m_chromosome + this.m_position; 
			 return combinedKey.hashCode();
		 }

	}
	
	public ArrayList<Locus> getSortedLoci() throws ClassCastException, UnsupportedOperationException
	{
		Collections.sort(loci);
		return loci;
	}
	
	public void addLocus(String chromosome, int position, int maxAlleleSize)
	{
		Locus newLocus = new Locus (chromosome, position);
		if (!this.loci.contains(newLocus))
		{
			this.loci.add(newLocus);
			this.maxAlleleSize.put(newLocus, maxAlleleSize);
		}
		else if(this.maxAlleleSize.get(newLocus) < maxAlleleSize)
		{
			this.maxAlleleSize.put(newLocus, maxAlleleSize);
		}
	}
	
	/**
	 * Retrieve the maximum number of characters that appear for the alleles at
	 * the given locus.
	 * @pre locus != null;
	 * @post true;
	 * @param locus - the locus to get the length of the longest allele.
	 */
	public int getMaxAlleleSize(Locus locus)
	{
		return this.maxAlleleSize.get(locus);
	}
	
	public int getPloidy ()
	{
		return this.ploidy;
	}
}
