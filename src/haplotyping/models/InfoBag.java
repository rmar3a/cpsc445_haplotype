package haplotyping.models;

import java.util.ArrayList;

/**
 * Class to pass the VCF information along with the genome information more
 * easily between the different views.
 */
public class InfoBag
{
	public ArrayList<VCFInfo> childFiles;
	public VCFInfo dadFile;
	public VCFInfo momFile;
	public Genome genome; 
	
	/**
	 * Constructor for the InfoBag.
	 * @pre childFiles != null && dadFile != null && momFile != null && genome != null;
	 * @post true;
	 * @param childFiles - the VCFInfos of the children.
	 * @param dadFile - the VCFInfo of the imputed dad.
	 * @param momFile - the VCFInfo of the imputed mom.
	 */
	public InfoBag(ArrayList<VCFInfo> childFiles,
				   			  VCFInfo dadFile,
				   			  VCFInfo momFile,
				   			   Genome genome)
	{
		this.childFiles = childFiles;
		this.dadFile = dadFile;
		this.momFile = momFile;
		this.genome = genome;
	}
}
