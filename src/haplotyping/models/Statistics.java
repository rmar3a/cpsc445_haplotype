package haplotyping.models;

/**
 * Class to hold the statistical information of the data extracted from the
 * VCF files.  Includes information obtained from running the Preprocessor
 * and the Haplotype information.
 */
public class Statistics
{
	/**
	 * Emtpy constructor for the Statistics.
	 * @pre true;
	 * @post true;
	 */
	public Statistics()
	{
		// empty constructor
	}
}
