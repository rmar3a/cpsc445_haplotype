package haplotyping.models;

import haplotyping.models.AlleleInfo.Haplotype;
import haplotyping.models.Genome.Locus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * Class to hold information extracted from VCF files.
 */
public class VCFInfo
{
	/**
	 * enum for describing relation of individual for a cross.
	 */
	public enum Relation
	{
		MOM, DAD, CHILD
	}
	
	public static final String MISSING_ALLELE = ".";
	
	private String familyID;
	private String individualID;
	//private HashMap<Locus, ArrayList<String>> locusAlleleMap; // TODO:  we'll need to iterate through the keys in sorted order later.  Should we use a sorted TreeMap instead?
	private HashMap<Locus, AlleleInfo> locusAlleleInfoMap; // TODO:  we'll need to iterate through the keys in sorted order later.  Should we use a sorted TreeMap instead?
	private Relation relation;

	
	/**
	 * Empty constructor for VCFInfo.
	 * @pre true;
	 * @post true;
	 */
	public VCFInfo()
	{
		locusAlleleInfoMap  = new HashMap<Locus, AlleleInfo>();
	}
	

	/**
	 * Retrieve the Family ID.
	 * @pre true;
	 * @post true;
	 * @return the Family ID for the VCF info.
	 */
	public String getFamilyID()
	{
		return this.familyID;
	}

	/**
	 * Set the Family ID.
	 * @pre familyID != null;
	 * @post this.familyID.equals(familyID);
	 * @param familyID - the Family ID to set for the VCF info.
	 */
	public void setFamilyID(String familyID)
	{
		this.familyID = familyID;
	}

	/**
	 * Retrieve the Individual ID.
	 * @pre true;
	 * @post true;
	 * @return the Individual ID.
	 */
	public String getIndividualID()
	{
		return this.individualID;
	}

	/**
	 * Set the Individual ID.
	 * @pre individualID != null;
	 * @post this.individualID.equals(individualID);
	 * @param individualID - the Individual ID to set for the VCF info.
	 */
	public void setIndividualID(String individualID)
	{
		this.individualID = individualID;
	}

	/**
	 * Retrieve the location of the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public ArrayList<String> getLocusAlleles(String chromosome, int position)
	{
		Genome.Locus locus = new Genome.Locus(chromosome, position);
		if (locusAlleleInfoMap.containsKey(locus))
		{
			System.out.println("found key" + locus);
		}
		else
		{
			System.out.println("not found key" + locus);
		}
		return this.locusAlleleInfoMap.get(locus).getUnphasedAlleles();
	}
	
	/**
	 * Retrieve the location of the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public AlleleInfo getLocusAlleleInfo(Genome.Locus locus)
	{
		return this.locusAlleleInfoMap.get(locus);
	}
	
	/**
	 * Retrieve the location of the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public ArrayList<String> getLocusAlleles(Genome.Locus locus)
	{
		AlleleInfo info = this.locusAlleleInfoMap.get(locus);
		
		if(info != null)
		{
			return info.getUnphasedAlleles();
		}
		
		return null;
	}
	
	/**
	 * Set the location and the allele in the genome.
	 * @pre true;
	 * @post true;
	 * @return the location of the individual in the genome.
	 */
	public void addLocusAlleles(Genome.Locus locus, ArrayList<String> alleles)
	{
			AlleleInfo alleleInfo = new AlleleInfo(alleles);
			this.locusAlleleInfoMap.put(locus, alleleInfo);
	}

	/**
	 * Set the location of the allele in the genome.
	 * @pre location != null;
	 * @post this.location.equals(location);
	 * @param location - the location to set for the VCF info.
	 */
	public void addLocusAlleles(String chromosome, int position, ArrayList<String> alleles)
	{
		Genome.Locus locus = new Genome.Locus(chromosome, position);
		AlleleInfo alleleInfo = new AlleleInfo(alleles);
		this.locusAlleleInfoMap.put(locus, alleleInfo);
	}

	
	/**
	 * Retrieve the relation of the individual in the cross.
	 * @pre true;
	 * @post true;
	 * @return the relation of the individual in the cross.
	 */
	public Relation getRelation()
	{
		return this.relation;
	}

	/**
	 * Set the relation of the individual in the cross.
	 * @pre relation != null;
	 * @post this.relation.equals(relation);
	 * @param relation - the relation to set for the VCF info.
	 */
	public void setRelation(Relation relation)
	{
		this.relation = relation;
	}
	
	/**
	 * Sets the haplotypes for the given alleles
	 * @pre
	 * @post
	 * @param locus
	 * @param momAllele
	 * @param momAllelePhase
	 * @param dadAllele
	 * @param dadAllelePhase
	 */
	public void setHaplotype(Locus locus, String firstAllele, Haplotype firstAlleleHaplotype, String secondAllele, Haplotype secondAlleleHaplotype)
	{
		AlleleInfo alleleinfo;
		if (!locusAlleleInfoMap.containsKey(locus))
		{
			alleleinfo = new AlleleInfo(firstAllele, firstAlleleHaplotype, secondAllele, secondAlleleHaplotype);
		}
		else 
		{
			alleleinfo = locusAlleleInfoMap.get(locus);
			alleleinfo.setPhasing(firstAllele, firstAlleleHaplotype, secondAllele, secondAlleleHaplotype);
		}
		this.locusAlleleInfoMap.put(locus, alleleinfo);
	}

	/**
	 * 
	 * @pre
	 * @post
	 * @param loci
	 * @return  Genotype at given loci.  If there are missing values at any of the loci, returns null.
	 */
	public MultiLocusGenotype getGenotype(ArrayList<Locus> loci)
	{
		MultiLocusGenotype genotype  = new MultiLocusGenotype();
		for (Locus locus: loci)
		{	
			ArrayList<String> alleles = null;
			
			AlleleInfo alleleInfo = this.locusAlleleInfoMap.get(locus);
			
			if(alleleInfo != null)
			{
				alleles = alleleInfo.getUnphasedAlleles();
			}
			
			if (alleles == null || alleles.size() == 0)
			{
				return null;
			}
			genotype.addLocusAlleles(locus, alleles);
		}
		return genotype;		
	}
	
	/**
	 * 
	 * @pre
	 * @post
	 */
	public void printHumanFriendly()
	{
		System.out.println("Individual ID = " + this.getIndividualID());
		System.out.println("Family ID = " + this.getFamilyID());
		System.out.println("Relation = " + this.getRelation());
		
		Set<Locus> lociKeysSet = locusAlleleInfoMap.keySet();
		Locus[] lociArr = lociKeysSet.toArray(new Locus[lociKeysSet.size()]);
		Arrays.sort(lociArr);
		for (Locus locus : lociArr)
		{
			String alleles = StringUtils.join(locusAlleleInfoMap.get(locus).getUnphasedAlleles(), '/');
			System.out.println("Locus: " + locus.toString() + " Unphased Alleles: " + alleles);
		}
	}
	
	public static String getAllelesAsString(ArrayList<String> alleles)
	{
		return StringUtils.join(alleles, '/');
	}
}
