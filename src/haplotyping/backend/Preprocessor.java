package haplotyping.backend;

import java.util.ArrayList;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.MultiLocusGenotype;
import haplotyping.models.VCFInfo;

/**
 * Class to perform the preprocessing of the VCF file sequence information.
 * i.e. fill in the missing alleles where possible.
 */
public class Preprocessor
{
	Genome genome;
	/**
	 * Emtpy constructor for the Preprocessor.
	 * @pre true;
	 * @post true;
	 */
	public Preprocessor(Genome theGenome)
	{
		genome  = theGenome;
		LOGGER.setLevel(Level.OFF);
	}
	
	
	private static Logger LOGGER = Logger.getLogger(Preprocessor.class.getName());
	
	/**
	 * Fill in the missing allele information where possible.
	 * @pre extractedInfo != null;
	 * @post true;
	 * @param childInfo - VCFInfo containing the sequence information to
	 * 					  fill in if any missing data.
	 * @param dadInfo - VCFInfo containing the sequence information of the
	 * 					father of the child VCFInfo.
	 * @param momInfo - VCFInfo containing the sequence information of the
	 * 					mother of the child VCFInfo.
	 * @return the updated version of the child VCFInfo containing any
	 * 		   missing allele information that was retrievable.
	 */
	public static VCFInfo preprocessVCFInfo(VCFInfo childInfo, VCFInfo dadInfo, VCFInfo momInfo)
	{
		return null;
	}
	
	/**
	 * Determine how of the alleles are known.
	 * @pre first != null && second != null;
	 * @post true;
	 * @param first - one of the alleles to check.
	 * @param second - the other allele to check.
	 * @return the number of alleles that are known.
	 */
	public int numOfAllelesGiven(String first, String second)
	{
		int knownCount = 0;
		// to do: implementation that determines number of known alleles
		return knownCount;
	}
	
	/**
	 * Determine if the given alleles are homozygous or heterozygous.
	 * @pre first != null & second != null;
	 * @post true;
	 * @param first - one of the two alleles to compare.
	 * @param second - the other allele to compare.
	 * @return true if alleles are homozygous; false otherwise.
	 */
	public boolean isHomozygous(String first, String second)
	{
		return first.equals(second);
	}
	
	/**
	 * For a list of individuals, determine if they are all homozygous at the specified locus.
	 * Only handles 2N individuals
	 * @pre
	 * @post
	 * @param listVCFInfo
	 * @return false if any missing data, or if one individual is heterozygous, 
	 * or if they do not share same alleles
	 */
	private static boolean isHomozygousConsensusAtLocus (ArrayList<VCFInfo> listVCFInfo, Locus locus)
	{
		boolean isHomozygousConsensus = true;
		String lastAllele = null;
		for (VCFInfo vcfInfo : listVCFInfo)
		{
			AlleleInfo alleleInfo = vcfInfo.getLocusAlleleInfo(locus);
			
			if (alleleInfo == null || !alleleInfo.isHomozygous())
			{
				isHomozygousConsensus = false;
				break;
			}
			
			ArrayList<String> alleles = alleleInfo.getUnphasedAlleles();
			String currentAllele = alleles.get(0);
			
			if(lastAllele == null)
			{
				lastAllele = alleles.get(0);
			}

			if (!currentAllele.equalsIgnoreCase(lastAllele))
			{
				isHomozygousConsensus = false;
				break;
			}
			
			lastAllele = currentAllele;
		}
		
		return isHomozygousConsensus;
	}
	
	/**
	 * Find other alleles in the individuals that do not match the given allele at the given locus.  Assumes diploid.
	 * @pre
	 * @post
	 * @param listVCFInfo
	 * @return
	 */
	private static ArrayList<String> getAlternateAlleles (ArrayList<VCFInfo> listVCFInfo, String baseAllele, Locus locus) throws Exception
	{
		// Find the alleles in the VCFInfo individuals
		// ignore the first allele in the individual that is the same as the baseAllele
		HashSet<String> altAllelesUnique = new HashSet<String>() ;
		for (VCFInfo vcfinfo : listVCFInfo)
		{
			ArrayList<String> alleles = vcfinfo.getLocusAlleles(locus);
			if (alleles == null)
			{
				continue;
			}
			
			if (GeneticsHelper.isHomozygous(alleles) && alleles.indexOf(baseAllele) >= 0)
			{
				altAllelesUnique.add(baseAllele);
			}
			else
			{
				for (int i = 0; i < alleles.size(); i++)
				{
					if (!alleles.get(i).equals(baseAllele))
					{
						altAllelesUnique.add(alleles.get(i));
					}
				}
			}			
		}
		
		ArrayList<String> altAlleles = new ArrayList<String>(Genome.DIPLOID_N);
				
		if (altAllelesUnique.size() > Genome.DIPLOID_N)
		{
			throw new Exception ("At locus: "  + locus.toString() + 
					" Imputed alleles= " + StringUtils.join(altAllelesUnique, '/') + " But expected only " + Genome.DIPLOID_N + "alleles.");
		}
		else if (altAllelesUnique.size() == 1)
		{
			for (int i = 0; i < Genome.DIPLOID_N; i++)
			{
				altAlleles.addAll(altAllelesUnique);
			}
		}
		else
		{
			altAlleles.addAll(altAllelesUnique);
		}
		 
		return altAlleles;
	}
	/**
	 * For a loci where one parent has missing genotype, but the other parent is homozygous,
	 * we compute the missing parental genotype from the other parent and kids.
	 * 
	 * EG)  We know that the DAD must have genotype GT
	 * 
	 * MOM  DAD		Kid1	Kid2
	 * AA	??		AG		AT
	 * 
	 * TODO:  this is very specific to our data.  Need to rewrite this for any other pedigrees
	 * 
	 * @pre pseudoMoms != null && dad != null && mom != null && kids != null;
	 * @post dad and mom are populated with imputed information.
	 * @param pseudoMoms - VCF files of the moms from the same population as the crossed mom.
	 * @param imputedDad - the VCFInfo that will contain the imputed genotype of the dad.
	 * @param imputedMom - the VCFInfo that will contain the imputed genotype of the true mom.
	 * @param kids - the list of VCFInfo of the children from the cross between the dad and true mom.
	 */
	public void imputeParentGenotype (ArrayList<VCFInfo> pseudoMoms, VCFInfo imputedDad, VCFInfo imputedMom, ArrayList<VCFInfo> kids) throws  Exception
	{
		ArrayList<Locus> loci = this.genome.getSortedLoci();
		for (Locus locus : loci)
		{						
			if (isHomozygousConsensusAtLocus(pseudoMoms, locus))
			{
				String pseudoMomAllele = pseudoMoms.get(0).getLocusAlleles(locus).get(0); // any pseudo mom allele.  we choose the first
	
				if (!validateKidsGenotype(kids, locus))
				{
					throw new Exception ("There are more than 4 genotypes amongst kids at locus" + locus);
				}
				else if (pseudoMomAllele == null)
				{
					throw new Exception ("Pseudomom " + pseudoMoms.get(0).getIndividualID() + " has null Allele String");
				}
				// It's possible that all 3 pseudo moms are homozygous but do not share the same alleles as the actual mother.
				if (validateHomozygousParentAllele(pseudoMomAllele, kids, locus))
				{
					ArrayList<String> dadAlleles = getAlternateAlleles(kids, pseudoMomAllele, locus);
					if (dadAlleles != null && dadAlleles.size() > 0)
					{
						imputedDad.addLocusAlleles(locus, dadAlleles);
						LOGGER.debug("Able to impute dad's alleles at locus " + locus);
					}
					else
					{
						LOGGER.debug("Dad has empty alleles at locus " + locus);
					}
					ArrayList<String> momAlleles = new ArrayList<String>();
					momAlleles.add(pseudoMomAllele);
					momAlleles.add(pseudoMomAllele);
					imputedMom.addLocusAlleles(locus, momAlleles);
					
					
				}
				else
				{
					LOGGER.debug("Pseudo moms all homozygous, but none of the kids share their alleles at locus " + locus);
				}
			}
			else
			{	
				LOGGER.debug("Pseudo moms are not all homozygous at locus:  " + locus);
			}
		}
	}
	
	/**
	 * Impute missing kid genotype where mom is homozygous and dad has a complete genotype
	 * @pre
	 * @post kid is populated with imputed information
	 * @param pseudoMoms - VCF files of the moms from same population as crossed mom.
	 * @param dad
	 * @param imputedKid - VCFInfo for imputed kid
	 */
	public void imputeKidsGenotype (ArrayList<VCFInfo> pseudoMoms, ArrayList<VCFInfo> dad, VCFInfo imputedKid) throws Exception
	{
		ArrayList<Locus> loci = this.genome.getSortedLoci();
		for (Locus locus : loci)
		{
			if (isHomozygousConsensusAtLocus(pseudoMoms, locus))
			{
				VCFInfo mom = pseudoMoms.get(0);
				String homozygousMomAllele = mom.getLocusAlleles(locus).get(0);
				ArrayList<String> kidAlleles = getAlternateAlleles(dad, homozygousMomAllele, locus);
				
				imputedKid.addLocusAlleles(locus, kidAlleles);
			}
		}
	}
		
	/**
	 * Checks if the given parent allele is homozygous at a locus.
	 * If all the kids contain the given parent allele at the same locus, then that parent allele is considered homozygous.
	 * @pre
	 * @post
	 * @param pseudoMoms
	 * @param locus
	 * @return  true if all the kids contain the parent allele at the given locus.  false otherwise
	 */
	public static boolean validateHomozygousParentAllele(String parentAllele, ArrayList<VCFInfo> kids, Locus locus)
	{
		for (VCFInfo kid : kids)
		{
			ArrayList<String> kidAlleles = kid.getLocusAlleles(locus);
			if (kidAlleles != null && kidAlleles.indexOf(parentAllele) < 0)
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks that there are at most 4 genotypes in the kids at the given locus.
	 * @pre
	 * @post
	 * @param kids
	 * @param locus
	 * @return  true if there are at most 4 genotypes amongst the kids at the given locus.  false otherwise
	 */
	public boolean validateKidsGenotype(ArrayList<VCFInfo> kids, Locus locus)
	{
		HashSet<MultiLocusGenotype> genotypeSet = new HashSet<MultiLocusGenotype>(); 
		for (VCFInfo kid : kids)
		{
			ArrayList<String> kidAlleles = kid.getLocusAlleles(locus);
			if (kidAlleles != null)
			{
				MultiLocusGenotype genotype = new MultiLocusGenotype();
				genotype.addLocusAlleles(locus, kidAlleles);
				genotypeSet.add(genotype);
			}
		}
		if (genotypeSet.size() <= 4)
		{
			return true;
		}
		return false;

	}
}
