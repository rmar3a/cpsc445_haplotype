package haplotyping.backend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.apache.log4j.Logger;

import haplotyping.models.AlleleInfo;
import haplotyping.models.AlleleInfo.Haplotype;
import haplotyping.models.Genome.Locus;
import haplotyping.models.MultiLocusGenotype;
import haplotyping.models.VCFInfo;

/**
 * 
 * Finds the recombinant and parental genotypes between 2 adjacent loci.
 * Where the mom is homozygous, finds dad haplotype based on parental versus recombinant genotype.
 * Where the mom is homozygous, finds kid haplotype:
 * 		- Figure out dad allele by eliminating mom allele from kid genotype.
 * 		- Figure out kid haplotype by dad's haplotype.  If kid has recombinant genotype, 
 * 				then know that dad gave gamete with recombination between those adjacent loci.
 * 				If kid has parental genotype, then know dad gave gamete with no recombination between those adjacent loci.  
 *   
 * ASSUMES: more than 2 alleles are possible at any one site, but organism is only diploid.
 * 
 * CASE 1)  Mom homozygous both loci, Dad heterozygous both loci
 * 
 * Mom	Dad
 * GG	CT
 * AA	TA
 * 
 * If there is no recombination at all, then kids will have genotype ratios
 * 50%	:	50%
 * GT 		GC
 * AA 		AT
 * 
 * If there is recombination in dad (we won't notice recombination in homoygous mom), then the kids will have genotype ratios
 * higher than 25%:  higher than 25% : 	lower than 25%:	lower than 25%
 * GT 				GC					GC				GT
 * AA				AT					AA				AT
 * 
 * CASE 2)  Mom homozygous both loci, Dad heterozygous both loci.  Dad shares an allele with mom at both loci.
 * 
 * Mom	Dad
 * GG	GT
 * AA	AT
 * 
 * If there is no recombination at all, then kids will have genotype ratios
 * 50%	:	50%
 * GG 		GT
 * AA 		AT
 * 
 * If there is recombination in dad (we won't notice recombination in homoygous mom), then the kids will have genotype ratios
 * higher than 25%:  higher than 25% : 	lower than 25%:	lower than 25%
 * GG 				GT					GG				GT
 * AA				AT					AT				AA
 * 
 * CASE 3)  Mom homozygous at all loci, Dad heterozygous at one locus & homozygous at other locus.
 * 
 * Mom	Dad
 * GG	CC
 * AA	TA
 * 
 * If there is no recombination, we expect this ratio:
 * 50%	:	50%
 * GC 		GC
 * AA 		AT
 * 
 * 
 * CASE 4)  Mom homozygous both loci, Dad homozygous both loci
 * 
 * Mom	Dad
 * GG	CC
 * AA	TT
 * 
 * We can not detect any recombination in this case.  We expect kids to have genotype ratio
 * 100%
 * GC 	
 * AT 	
 * 
 * If there is recombination, then the frequencies of parental genotypes will increase
 * 	and the frequencies of recombinant genotypes will be decrease.
 * We use Chi-squre tests to determine if the genotype frequency ratios are significantly different from the expected ratios. 
 * 
 * In order to find haplotype at the current locus, we need to keep track of the haplotype at the previous locus.
 * The haplotype at the current locus should result in the least amount of recombination.
 * 
 * TODO:  only does this where the mom is homozygous.  Handle where mom is heterozygous.
 * 
 */
public class HomozygousParentRecombination
{
	
	private static Logger LOGGER = Logger.getLogger(HomozygousParentRecombination.class.getName());
	
	/**
	 * ASSUMES: 
	 * - diploid organism but at any locus, there can be more than 2 possible alleles.
	 * - progeny have at least one parent that is homozygous at the given locus  
	 *
	 * @pre
	 * @post
	 * @param locus
	 * @param progeny
	 * @param pvalue
	 * @return
	 * @throws Exception if the number of genotypes is 
	 */
//	//TODO:  refactor this.  Instead of the nested structures, we need to reuse an existing class (Genome?) or create another to hold alleles at multiple loci.
//	public ArrayList<ArrayList<Set<String>>> getRecombinantGenotypes( Locus firstLocus, Locus secondLocus, VCFInfo mom, VCFInfo dad, ArrayList< VCFInfo> kids, double pvalue) throws Exception
//	{
//		ArrayList<ArrayList<Set<String>>> recombinantKidGenotypes  = new ArrayList<ArrayList<Set<String>>>();
//		if (this.isRecombination(firstLocus, secondLocus, mom, dad, kids))
//		{
//			HashMap<ArrayList<Set<String>>, Long> genotypeToCounts = getGenotypeCounts(firstLocus,  secondLocus, kids);
//			int numUniqueKidGenotypes = genotypeToCounts.size();
//			
//			double noRecomboGenotypeCounts[]  = new double [numUniqueKidGenotypes];
//			int  numUniqueNoRecomboKidGenotypes = this.getNumUniqueNoRecomboKidGenotypes(firstLocus, secondLocus, mom, dad);
//			double noRecomboGenotypeCount = 1.0 / numUniqueNoRecomboKidGenotypes * kids.size();
//			Arrays.fill(noRecomboGenotypeCounts, 0, numUniqueKidGenotypes-1, noRecomboGenotypeCount);
//			
//			long observedGenotypeCounts[] = new long[numUniqueKidGenotypes];
//			int i = 0;
//			for (ArrayList<Set<String>> genotype : genotypeToCounts.keySet())
//			{
//				long observedGenotypeCount = genotypeToCounts.get(genotype);
//				observedGenotypeCounts[i] = observedGenotypeCount;
//				i++;
//			}
//			ChiSquareTest  significanceTest = new ChiSquareTest();
//			boolean isSignificantDifference = significanceTest.chiSquareTest(noRecomboGenotypeCounts, observedGenotypeCounts, pvalue);
			
			// If we assume that one parent is homozygous, then the recombinant genotypes will be the genotypes that occur at lower frequencies
//			int  numUniqueNoRecomboKidGenotypes = this.getNumUniqueNoRecomboKidGenotypes(firstLocus, secondLocus, mom, dad);
//			double expectedNoRecomboKidGenotypesFreq =  1 / numUniqueNoRecomboKidGenotypes;
//			for (ArrayList<Set<String>> genotype : genotypeToCounts.keySet())
//			{
//				long observedGenotypeCount = genotypeToCounts.get(genotype);
//				double observedGenotypeFreq = observedGenotypeCount  * 1.0 / numUniqueKidGenotypes;
//				if (observedGenotypeFreq < expectedNoRecomboKidGenotypesFreq)
//				{
//					recombinantKidGenotypes.add(genotype); 
//				}
//			}			
//		}		
//		
//		return recombinantKidGenotypes;
//	}
	
	
	/**
	 * ASSUMES: 
	 * - diploid organism but at any locus, there can be more than 2 possible alleles.
	 * - progeny have at least one parent that is homozygous at the given locus  
	 *
	 * @pre
	 * @post
	 * @param locus
	 * @param progeny
	 * @param pvalue
	 * @return Parental genotypes (the genotypes created from parental gametes with no recombination)
	 * @throws Exception if the number of genotypes is 
	 */
	public static ArrayList<MultiLocusGenotype> getNoRecomboGenotypes( Locus firstLocus, Locus secondLocus, VCFInfo mom, VCFInfo dad, ArrayList< VCFInfo> kids) throws Exception
	{
		ArrayList<MultiLocusGenotype> parentalGenotypes  = new ArrayList<MultiLocusGenotype>();
		HashMap<MultiLocusGenotype, Long> genotypeToCounts = getGenotypeCounts(firstLocus,  secondLocus, kids);
		
		if (isRecombination(firstLocus, secondLocus, mom, dad, kids))
		{
			int numUniqueKidGenotypes = genotypeToCounts.size();
			int totalIndivWithNonMissingGenotypes = 0;
			for (long count : genotypeToCounts.values())
			{
				totalIndivWithNonMissingGenotypes += count;
			}
			
			// If we got to this point, then we assume isRecomination() did checks on invalid genotypes
			// If we assume that one parent is homozygous, then the recombinant genotypes will be the genotypes that occur at lower frequencies
			double expectedMendelianGenotypeFreq =  1.0 / numUniqueKidGenotypes;
			for (MultiLocusGenotype genotype : genotypeToCounts.keySet())
			{
				long observedGenotypeCount = genotypeToCounts.get(genotype);
				double observedGenotypeFreq = observedGenotypeCount  * 1.0 / totalIndivWithNonMissingGenotypes;
				if (observedGenotypeFreq >= expectedMendelianGenotypeFreq)
				{
					parentalGenotypes.add(genotype); 
				}
			}			
		}	
		else
		{
			for (MultiLocusGenotype genotype: genotypeToCounts.keySet())
			{
				parentalGenotypes.add(genotype);
			}
			
		}
		
		if (LOGGER.isDebugEnabled())
		{
			StringBuilder str = new StringBuilder();
			str.append("Non-recombinant genotypes at loci: ").append(firstLocus).append("-").append(secondLocus).append("\n");
			for (MultiLocusGenotype genotype: parentalGenotypes)
			{
				str.append(genotype.toString()).append("\n");
			}
			
			LOGGER.debug( str.toString());	
		}
		return parentalGenotypes;
	}
	
	/**
	 * We expect the mom to be homozygous across all given loci.  We already know her haplotype because she is homozygous.
	 * For every kid, remove the mom haplotype from the genotype.  The remaining alleles make up the dad haplotype.
	 * @pre
	 * @post
	 * @return haplotype at given loci.
	 */
	// TODO:  use parental versus recombinant genotypes to determine which alleles belong to which gamete strand.
	public static void setPhaseAtLoci (Locus firstLocus, Locus secondLocus, VCFInfo momVCF, VCFInfo dadVCF, ArrayList< VCFInfo> kidsVCF ) throws Exception
	{
		ArrayList<Locus> loci = new ArrayList<Locus>();
		loci.add(firstLocus);
		loci.add(secondLocus);
		
		
		if (!GeneticsHelper.isHomozygous(loci, momVCF))
		{
			throw new Exception ("Expect mom to be homozygous across both loci, but she isn't. Loci=" + firstLocus + "-" + secondLocus);
		}
		
		// These are the genotypes that are produced when the parent gametes have no recombination		
		ArrayList<MultiLocusGenotype> noRecomboGenotypes = getNoRecomboGenotypes(firstLocus, secondLocus, momVCF, dadVCF, kidsVCF);
		
		
		//TODO:  we need to create separate classes for genotype and haplotype
		// TODO:  combine VCFInfo and MultiLocusGenotype into single class.

		// From the kid genotypes with no recombination, get the mom and dad haplotypes 
		for (MultiLocusGenotype kidGenotype: noRecomboGenotypes)
		{
			Haplotype lastDadHaplotypeGivenToKid = Haplotype.HAPLOTYPE_UNPHASED;
			Haplotype lastMomGameteHaplotype = Haplotype.HAPLOTYPE_UNPHASED;
			for(Locus locus:  loci)
			{				
				ArrayList<String> kidAlleles = kidGenotype.getLocusAlleles(locus);
				ArrayList<String> momAlleles = momVCF.getLocusAlleles(locus);
				ArrayList<String> dadAlleles = dadVCF.getLocusAlleles(locus);
				
				//The gamete alleles are the alleles the parent gave to the kid
				String momAlleleGivenToKid = momAlleles.get(0);		
				String momAlleleNotGivenToKid = momAlleleGivenToKid;
				String dadAlleleGivenToKid = GeneticsHelper.getAlternateAllele(kidAlleles, momAlleleGivenToKid);
				String dadAlleleNotGivenToKid = GeneticsHelper.getAlternateAllele(dadAlleles, dadAlleleGivenToKid);
				
				if (!momVCF.getLocusAlleleInfo(locus).isPhased())
				{
					momVCF.setHaplotype(locus, momAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS, momAlleleNotGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS);
				}
								
				
				if (!dadVCF.getLocusAlleleInfo(locus).isPhased()) 
				{
					if (GeneticsHelper.isHomozygous(dadAlleleGivenToKid, dadAlleleNotGivenToKid))
					{
						dadVCF.setHaplotype(locus, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS, dadAlleleNotGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS);
					}
					else if (lastDadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_UNPHASED)
					{	// arbitrarily set dad allele haplotypes at this locus if previous locus has no haplotype
						dadVCF.setHaplotype(locus, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_1, dadAlleleNotGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_2);
					}
					else if (lastDadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_FROM_DAD_1 )
					{
						dadVCF.setHaplotype(locus, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_1, dadAlleleNotGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_2);
					}
					else
					{
						dadVCF.setHaplotype(locus, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_2, dadAlleleNotGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_1);
					}
				}
				
				lastDadHaplotypeGivenToKid = dadVCF.getLocusAlleleInfo(locus).getHaplotype(dadAlleleGivenToKid);
			}
		}
		
		// Now that we have the mom and dad haplotypes, get the kid haplotypes
		for (VCFInfo kidVCF : kidsVCF)
		{
			Haplotype lastDadHaplotype = Haplotype.HAPLOTYPE_UNPHASED;
			for (Locus locus : loci)
			{
				ArrayList<String> kidAlleles = kidVCF.getLocusAlleles(locus);
				ArrayList<String> momAlleles = momVCF.getLocusAlleles(locus);
				String momAlleleGivenToKid = momAlleles.get(0);		
				
				String dadAlleleGivenToKid = GeneticsHelper.getAlternateAllele(kidAlleles, momAlleleGivenToKid);
				Haplotype dadHaplotypeGivenToKid = dadVCF.getLocusAlleleInfo(locus).getHaplotype(dadAlleleGivenToKid);
				
				// TODO:   what if homozygous?   then which haplotype do we select?
				if (dadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_GIVEN_TO_KID_1 || 
						(dadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS && lastDadHaplotype == Haplotype.HAPLOTYPE_GIVEN_TO_KID_1))
				{
					// arbitrarily set mom's hapltotype to 1
					// TODO:  this will bite us later when we handle heterozygous sites
					kidVCF.setHaplotype(locus, momAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_FROM_DAD_1);
				}
				else if (dadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_GIVEN_TO_KID_2 ||
						(dadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS && lastDadHaplotype == Haplotype.HAPLOTYPE_GIVEN_TO_KID_2))
				{
					kidVCF.setHaplotype(locus, momAlleleGivenToKid, Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS, dadAlleleGivenToKid, Haplotype.HAPLOTYPE_FROM_DAD_2);
				}
				else if (dadHaplotypeGivenToKid == Haplotype.HAPLOTYPE_UNPHASED)
				{
					throw new Exception ("We expect dad to be haplotyped at locus " + locus + " but he isn't");
				}
				
				if (lastDadHaplotype == Haplotype.HAPLOTYPE_UNPHASED)
				{
					lastDadHaplotype = dadHaplotypeGivenToKid;
				}
				else if (dadHaplotypeGivenToKid != Haplotype.HAPLOTYPE_GIVEN_TO_KID_HOMOZYGOUS && dadHaplotypeGivenToKid != Haplotype.HAPLOTYPE_UNPHASED)
				{
					lastDadHaplotype =   dadHaplotypeGivenToKid;
				}
				
			}
		}
	}
	
	/**
	 * Uses the number of genotypes amongst the kids to determine if there is recombination between the loci.
	 * ASSUMES:  one of parents is homozygous at both loci
	 * @pre
	 * @post
	 * @param firstLocus
	 * @param secondLocus
	 * @param mom
	 * @param dad
	 * @param kids
	 * @return  true if there is recombination between the 2 loci in the kids
	 */
	//TODO:  handle situations where there is recombination, but one of the recombinant genotypes is deleterious, causing only only one recombinant genotype to show up
	public static boolean isRecombination (Locus firstLocus, Locus secondLocus, VCFInfo mom, VCFInfo dad, ArrayList<VCFInfo> kids) throws Exception
	{
		boolean isRecombo = false;
		
		ArrayList<Locus> loci = new ArrayList<Locus>();
		loci.add(firstLocus);
		loci.add(secondLocus);
		
		if (!GeneticsHelper.hasAlleles(loci, mom) || !GeneticsHelper.hasAlleles(loci, dad) )
		{
			throw new Exception ("Mom or Dad are missing alleles at the given loci " + firstLocus + "-" + secondLocus);
		}
		
		HashMap<MultiLocusGenotype, Long> genotypeToCounts = getGenotypeCounts(firstLocus,  secondLocus, kids);		
		int numGenotypes = genotypeToCounts.size();
		
		boolean isMomHomoBothLoci =  GeneticsHelper.isHomozygous(loci, mom);
		boolean isDadHomoBothLoci =  GeneticsHelper.isHomozygous(loci, dad);
		
		if (numGenotypes == 0)
		{
			throw new Exception ("Unable to detect recombination amongst kids.  " + 
					"There are no genotypes at the given loci: " + firstLocus + " - " + secondLocus);
		}
		else if (isMomHomoBothLoci && isDadHomoBothLoci)	
		{	
			if ( numGenotypes > 1)
			{
				throw new Exception ("Invalid genotype count at loci = " + firstLocus.toString() + " "  +  secondLocus + "\n" + 
						"For progeny where both parents are homozygous at both loci, we expect 1 progeny genotype.\n ");
			}
			else
			{
				isRecombo = true;
			}
		}
		else if (isMomHomoBothLoci || isDadHomoBothLoci )
		{
			if (numGenotypes == 4) //expect 4 kid genotypes if no recombination
			{
				isRecombo = true;
			}
			else if (numGenotypes == 2)  //expect 2 kid genotypes if there is recombination
			{
				isRecombo = false;
			}
			else
			{
				throw new Exception ("Invalid genotype count at loci = " + firstLocus.toString() + " "  +  secondLocus + "\n" + 
						".  For progeny where only 1 parent is homozygous at both loci, we expect 2 progeny genotypes if no recombination, 4 if there is." +
						"  Num of genotypes in kids = " + numGenotypes + "\n ");
			}			
		}
		else
		{
			throw new Exception ("This method expects at least one parent to be homozygous\n");
		}
		
		if (isRecombo)
		{
			LOGGER.debug("Recombination in kids between loci: " + firstLocus + "-" + secondLocus);
		}
		else
		{
			LOGGER.debug("NO Recombination in kids between loci: " + firstLocus + "-" + secondLocus);
		}
	
		return isRecombo;
	}
	
	
	/**
	 * Returns the number of genotypes expected amongst the kids if there is no recombination.
	 * 
	 * @pre
	 * @post
	 * @param firstLocus
	 * @param secondLocus
	 * @param mom
	 * @param dad
	 * @param kids
	 * @return  the number of unique genotypes expected amongst the kids if there is no recombination.
	 */
	public static int getNumUniqueMendelianKidGenotypes (Locus firstLocus, Locus secondLocus, VCFInfo mom, VCFInfo dad) throws Exception
	{	
		ArrayList<Locus> loci = new ArrayList<Locus>();
		loci.add(firstLocus);
		loci.add(secondLocus);
		boolean isMomHomoBothLoci = GeneticsHelper.isHomozygous(loci, mom);
		boolean isDadHomoBothLoci = GeneticsHelper.isHomozygous(loci, dad);
		if (isMomHomoBothLoci && isDadHomoBothLoci)	
		{	
			return 1;
		}
		else if ((!isMomHomoBothLoci && isDadHomoBothLoci) || (isMomHomoBothLoci && !isDadHomoBothLoci))
		{	
			return 2;
		}
		else
		{
			return 4;
		}	
	}
	
	
	/**
	 * Returns  hashmap of the genotype (in AlleleInfo object) to counts of that genotype amongst the given individuals.
	 * If any of the individuals are missing any alleles for any loci, then they are not included in the genotype counts
	 * @pre
	 * @post
	 * @param locus
	 * @return
	 */
	public static HashMap<MultiLocusGenotype, Long> getGenotypeCounts ( Locus firstLocus, Locus secondLocus,  ArrayList<VCFInfo> individuals)
	{
		HashMap<MultiLocusGenotype, Long> genotypeToCounts = new HashMap<MultiLocusGenotype, Long>();
		
		ArrayList<Locus> loci = new ArrayList<Locus>();
		loci.add(firstLocus);
		loci.add(secondLocus);
		for (VCFInfo indiv : individuals)
		{			
			MultiLocusGenotype genotype =  indiv.getGenotype(loci);
			
			if (genotype == null)
			{
				continue;
			}
			
			if (!genotypeToCounts.containsKey(genotype))
			{
				genotypeToCounts.put(genotype, Long.valueOf(1));
			}
			else
			{
				long genotypeCount = genotypeToCounts.get(genotype);
				genotypeToCounts.put(genotype, genotypeCount + 1);
			}			
		}
		
		return genotypeToCounts;
	}
	
	
	
}
