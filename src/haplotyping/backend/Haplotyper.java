package haplotyping.backend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.VCFInfo;

public class Haplotyper
{

	/**
	 * Modifies the VCFInfo for the mom, dad, and kids and sets the phasing for the alleles at all loci
	 * @pre
	 * @post
	 * @param mom
	 * @param dad
	 * @param kids
	 * @param genome
	 */
	public static void haplotype(VCFInfo mom, VCFInfo dad, ArrayList<VCFInfo> kids, Genome genome) throws Exception
	{
		ArrayList<Locus> loci = genome.getSortedLoci();
		for (int i = 1; i < loci.size(); i++)
		{
			Locus lastLocus = loci.get(i-1);
			Locus locus = loci.get(i);
			ArrayList<Locus> twoLoci  = new ArrayList<Locus>();
			twoLoci.add(lastLocus);
			twoLoci.add(locus);
			// Uses minimum recombination within a chromosome to figure out haplotypes.
			// Both loci must be on same chromosome.
			// TODO:  for now, it only handles locations where one of the parents is homozygous
			if (lastLocus.getChromosome().equals(locus.getChromosome())  && 
					(GeneticsHelper.isHomozygous(twoLoci, mom)  || GeneticsHelper.isHomozygous(twoLoci, dad)) &&
					GeneticsHelper.hasAlleles(twoLoci, mom) && GeneticsHelper.hasAlleles(twoLoci, dad))
			{
				HomozygousParentRecombination.setPhaseAtLoci(lastLocus, locus, mom, dad, kids);
			}
		}
	}
	

	/**
	 * Prints the haplotypes to STDOUT
	 * @pre
	 * @post
	 * @param mom
	 * @param dad
	 * @param kids
	 * @param genome
	 */
	public static void printHaplotypes (VCFInfo mom, VCFInfo dad, ArrayList<VCFInfo> kids, Genome genome)
	{
		String missingAlleles = "?/?";
		ArrayList<Locus> loci = genome.getSortedLoci();
		for (Locus locus: loci)
		{	
			StringBuilder line = new StringBuilder(locus.toString());
			line.append(": ");
			
			AlleleInfo momAlleles = mom.getLocusAlleleInfo(locus); 
			line.append(" Mom=");
			if (momAlleles == null)
			{
				line.append(missingAlleles);
			}
			else 
			{
				if (momAlleles.toString() == null)
				{
					//noop 
					System.out.println("test breakpoint");
				}
				line.append(momAlleles.toString());
			}
			
			AlleleInfo dadAlleles = dad.getLocusAlleleInfo(locus);
			line.append(" Dad=");
			if (dadAlleles == null)
			{
				line.append(missingAlleles);
			}
			else
			{
				line.append(dadAlleles.toString());
			}
			
			for (int i = 0; i < kids.size(); i++)
			{
				AlleleInfo kidAlleles = kids.get(i).getLocusAlleleInfo(locus);
				line.append(" Kid").append(i).append("=");
				if (kidAlleles == null)
				{
					line.append(missingAlleles);
				}
				else
				{
					line.append(kidAlleles.toString());
				}
			}
			System.out.println(line);
		}
	}
}
