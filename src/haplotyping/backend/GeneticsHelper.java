package haplotyping.backend;

import haplotyping.models.Genome;
import haplotyping.models.MultiLocusGenotype;
import haplotyping.models.VCFInfo;
import haplotyping.models.Genome.Locus;

import java.util.ArrayList;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;

public class GeneticsHelper
{

	/**
	 * 
	 * @pre
	 * @post
	 * @param loci
	 * @param indiv
	 * @return  whether the individual has non-empty alleles at all given loci
	 */
	public static boolean hasAlleles(ArrayList<Locus> loci, VCFInfo indiv)
	{
		for (Locus locus: loci)
		{
			if (indiv.getLocusAlleles(locus) == null || indiv.getLocusAlleles(locus).size() == 0)
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Determine if the given alleles are homozygous or heterozygous.
	 * @pre first != null & second != null;
	 * @post true;
	 * @param first - one of the two alleles to compare.
	 * @param second - the other allele to compare.
	 * @return true if alleles are homozygous; false otherwise.
	 */
	public static boolean isHomozygous(String first, String second)
	{
		return first.equals(second);
	}
	
	/**
	 * Determine if the given alleles are homozygous or heterozygous.
	 * @pre first != null & second != null;
	 * @post true;
	 * @param first - one of the two alleles to compare.
	 * @param second - the other allele to compare.
	 * @return true if alleles are homozygous; false otherwise.
	 */
	public static boolean isHomozygous(ArrayList<String> alleles)
	{
		if(alleles == null)
		{
			return false;
		}
		
		String lastAllele = null;
		for (String allele : alleles)
		{
			if (lastAllele != null && !allele.equals(lastAllele))
			{
				return false;
			}
			lastAllele = allele;
		}
		return true;
	}
	
	/**
	 * 
	 * @pre
	 * @post
	 * @param loci
	 * @param individual
	 * @return  whether the individual is homozygous at all specified loci
	 */
	public static boolean isHomozygous (ArrayList<Locus> loci, VCFInfo individual)
	{
		boolean isLastLocusHomozygous = true;
		for (Locus locus : loci)
		{
			isLastLocusHomozygous = isLastLocusHomozygous && isHomozygous(individual.getLocusAlleles(locus));
		}
		return isLastLocusHomozygous;
	}
	
	/**
	 * For a list of individuals, determine if they are all homozygous at the specified locus.
	 * Only handles 2N individuals
	 * @pre
	 * @post
	 * @param listVCFInfo
	 * @return false if any missing data, or if one individual is heterozygous, 
	 * or if they do not share same alleles
	 */
	public static boolean isHomozygousConsensusAtLocus (ArrayList<VCFInfo> listVCFInfo, Locus locus)
	{
		boolean isHomozygousConsensus = true;
		String lastAllele = null;
		for (VCFInfo vcfInfo : listVCFInfo)
		{
			ArrayList<String> alleles = vcfInfo.getLocusAlleles(locus);
			if (alleles == null || alleles.size() < 1)
			{
				isHomozygousConsensus = false;
				break;
			}
			String firstAllele = alleles.get(0);
			String secondAllele = alleles.get(1);
			lastAllele = firstAllele;
			if (firstAllele.equalsIgnoreCase(secondAllele))
			{
				if (!firstAllele.equalsIgnoreCase(secondAllele)  || VCFInfo.MISSING_ALLELE.equalsIgnoreCase(firstAllele) || !firstAllele.equalsIgnoreCase(lastAllele))
				{
					isHomozygousConsensus = false;
					break;
				}
				lastAllele = firstAllele;
			}
		}
		return isHomozygousConsensus;
	}
	
	/**
	 * Find other partner alleles in the individuals for the baseAllele at the given locus.
	 * @pre
	 * @post
	 * @param listVCFInfo
	 * @return Unique set of partner alleles for the baseAllele in every VCFInfo individual at the given locus
	 */
	public static HashSet<String> getAlternateAlleles (ArrayList<VCFInfo> listVCFInfo, String baseAllele, Locus locus) throws Exception
	{
		// Find the alleles in the VCFInfo individuals
		// ignore the first allele in the individual that is the same as the baseAllele
		HashSet<String> altAllelesUnique = new HashSet<String>() ;
		for (VCFInfo vcfinfo : listVCFInfo)
		{
			ArrayList<String> alleles = vcfinfo.getLocusAlleles(locus);
			if (alleles == null || alleles.size() == 0)
			{
				continue;
			}
			
			String altAllele = getAlternateAllele(alleles, baseAllele);
			altAllelesUnique.add(altAllele);			
		}
		
		return altAllelesUnique;
	}
	
	
	/**
	 * Removes the first occurence of the baseAllele in the list.  Returns the remaining allele.  This is the partner allele for the baseAllele.
	 * @pre  Diploid alleles.  i.e.  2 elements in alleles
	 * @post
	 * @param listVCFInfo
	 * @return  Return the other allele in the ArrayList that is not the baseAllele.
	 */
	public static String getAlternateAllele (ArrayList<String> alleles, String baseAllele)
	{	
		ArrayList<String> copyAlleles = (ArrayList<String>)alleles.clone();
		copyAlleles.remove(baseAllele);
		return copyAlleles.get(0);
	}

}
