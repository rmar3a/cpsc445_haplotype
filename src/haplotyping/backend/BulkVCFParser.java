package haplotyping.backend;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Class to take a text file and parse out the list of paths to VCF files.
 * 
 * Required format of the text file:
 * Path
 * <path_to_VCF_files>
 * Moms
 * VCF_filename_for_mom_1
 * ...
 * VCF_filename_for_mom_n
 * Children
 * VCF_filename_for_child_1
 * ...
 * VCF_filename_for_child_n
 */
public class BulkVCFParser
{
	private static final String MOMS_TAG = "Moms";
	private static final String CHILDREN_TAG = "Children";
	private static final String PATH_TAG = "Path";
	private ArrayList<String> momFiles;
	private ArrayList<String> childFiles;
	
	/**
	 * Constructor for the BulkVCFParser.
	 * @pre true;
	 * @post true;
	 */
	public BulkVCFParser()
	{
		this.momFiles = new ArrayList<String>();
		this.childFiles = new ArrayList<String>();
	}
	
	/**
	 * Extract the file paths for the mom and child VCF files from the given bulk file. 
	 * @pre bulkFilePath != null;
	 * @post true;
	 * @param bulkFilePath - the path to the bulk file containing the paths to
	 * 						 the VCF files to import.
	 */
	public void parseBulkFile(String bulkFilePath)
	{
		this.resetLists();
		
		try
		{
			FileInputStream fStream = new FileInputStream(bulkFilePath);
			DataInputStream dStream = new DataInputStream(fStream);
			BufferedReader bReader = new BufferedReader(new InputStreamReader(dStream));
			String readLine;
			
			boolean parsingPath = false;
			boolean parsingMoms = false;
			boolean parsingChildren = false;
			String basePath = null;
			
			while((readLine = bReader.readLine()) != null)
			{
				if(readLine.trim().isEmpty())
				{
					// do nothing and skip line
				}
				else if(readLine.trim().equals(PATH_TAG))
				{
					parsingPath = true;
					parsingMoms = false;
					parsingChildren = false;
				}
				else if(readLine.trim().equals(MOMS_TAG))
				{
					parsingMoms = true;
					parsingChildren = false;
					parsingPath = false;
				}
				else if(readLine.trim().equals(CHILDREN_TAG))
				{
					parsingChildren = true;
					parsingMoms = false;
					parsingPath = false;
				}
				else
				{
					if(parsingMoms)
					{
						this.momFiles.add(basePath + readLine);
					}
					else if(parsingChildren)
					{
						this.childFiles.add(basePath + readLine); 
					}
					else if(parsingPath)
					{
						basePath = readLine.trim();
					}
				}
			}
	
			dStream.close();
		}
		catch(IOException e)
		{
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * Helper method to clear the lists for the mom and child files.
	 * @pre true;
	 * @post this.childFiles.size() == 0 && this.momFiles.size() == 0;
	 */
	private void resetLists()
	{
		this.childFiles.clear();
		this.momFiles.clear(); 
	}
	
	/**
	 * Retrieve the list of mom files parsed from the bulk file.
	 * @pre true;
	 * @post true;
	 * @return the ArrayList containing the mom file paths extracted from the bulk file.
	 */
	public ArrayList<String> getMomFiles()
	{
		return this.momFiles;
	}
	
	/**
	 * Retrieve the list of child files parsed from the bulk file.
	 * @pre true;
	 * @post true;
	 * @return the ArrayList containing the child file paths extracted from the bulk file.
	 */
	public ArrayList<String> getChildFiles()
	{
		return this.childFiles;
	}
}
