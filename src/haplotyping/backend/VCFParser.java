package haplotyping.backend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import haplotyping.models.AlleleInfo;
import haplotyping.models.Genome;
import haplotyping.models.Genome.Locus;
import haplotyping.models.VCFInfo;
import haplotyping.models.VCFInfo.Relation;

// GATK packages
import org.broad.tribble.AbstractFeatureReader;
import org.broad.tribble.FeatureReader;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.variantcontext.Allele;
import org.broadinstitute.sting.utils.variantcontext.Genotype;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

/**
 * Class to take a VCF file and extract the information into a VCFInfo.
 */
public class VCFParser
{
	/**
	 * Emtpy constructor for the VCFParser.
	 * @pre true;
	 * @post true;
	 */
	public VCFParser()
	{
	}
	
	/**
	 * Extract the information from a VCF file and retrieve the values of
	 * interest.
	 * @pre filePath != null;
	 * @post true;
	 * @param filePath - the file path to the VCF file.
	 * @return the VCFInfo containing the information from the VCF file.
	 * @throws IOException when there is a problem reading the file from the given file path.
	 */
	public static ArrayList<VCFInfo> extractInfo (String filePath) throws IOException
	{
		return extractInfo(filePath, null);
	}
	
	
	/**
	 * Extract the information from a VCF file and retrieve the values of
	 * interest.
	 * @pre filePath != null;
	 * @post true;
	 * @param filePath - the file path to the VCF file.
	 * @param genome - the genome object to populate with the allele information.
	 * @return the VCFInfo containing the information from the VCF file.
	 * @throws IOException when there is a problem reading the file from the given file path.
	 */
	public static ArrayList<VCFInfo> extractInfo (String filePath, Genome genome) throws IOException
	{
		return extractInfo(filePath, genome, null);
	}
	
	/**
	 * Extract the information from a VCF file and retrieve the values of
	 * interest.
	 * @pre filePath != null;
	 * @post true;
	 * @param filePath - the file path to the VCF file.
	 * @param genome - the genome object to populate with the allele informaiton.
	 * @param relation - the type of family member the VCF file is for.
	 * @return the VCFInfo containing the information from the VCF file.
	 * @throws IOException when there is a problem reading the file from the given file path.
	 */
	public static ArrayList<VCFInfo> extractInfo (String filePath, Genome genome, Relation relation) throws IOException
	{
		FeatureReader<VariantContext> reader = null;

		ArrayList<VCFInfo> extractedInfos = new ArrayList<VCFInfo>();

		// Code partially taken from http://plindenbaum.blogspot.fr/2012/11/readingwriting-vcf-file-with-gatk-api.html
		
		final VCFCodec vcfCodec = new VCFCodec();
		/** we don't need some indexed VCFs */
		boolean requireIndex=false;

		/* get A VCF Reader */
		reader = AbstractFeatureReader.getFeatureReader(filePath, vcfCodec, requireIndex);
		/* read the header */
		VCFHeader header = (VCFHeader)reader.getHeader();
		ArrayList<String> sampleNames = header.getSampleNamesInOrder();
		
		
		for (String sampleName: sampleNames)
		{
			VCFInfo extractedInfo  = new VCFInfo();
			if(relation != null)
			{
				extractedInfo.setRelation(relation);
			}
			extractedInfo.setIndividualID(sampleName);
			extractedInfos.add(extractedInfo);
		}
		
		/* loop over each Variation.  Right now, we store the entire VCF for each individual into memory.
		 * TODO:  after we verify functionality of algo using a few individuals, we need to buffer how many loci are read at once.  
		 * If we continue to use min recombinant approach, we should only read in a few loci at a time.  
		 */
		Iterator<VariantContext> it = reader.iterator();

		while ( it.hasNext() )
		{
			/* get next variation and save it */
			VariantContext vc = it.next();

			String chromosome = vc.getChr();
			int position = vc.getStart();	// Start position on the reference
			
			for (int i = 0; i < sampleNames.size(); i++)
			{
				VCFInfo extractedInfo = extractedInfos.get(i);
				Genotype genotype = vc.getGenotype(sampleNames.get(i));
				List<Allele> alleles = genotype.getAlleles();				
				ArrayList<String> strAlleles = new ArrayList<String>();				
				for (Allele allele: alleles)
				{
					strAlleles.add(allele.getBaseString());
				}
				
				extractedInfo.addLocusAlleles(chromosome, position, strAlleles);
				if (genome != null)
				{
					int maxAlleleSize = 0;
					// for loop to find max alleles size
					for(String allele : strAlleles)
					{
						maxAlleleSize = Math.max(maxAlleleSize, allele.length());
					}
					
					genome.addLocus(chromosome, position, maxAlleleSize);
				}					
			}	
		}
		
		reader.close();
		
		return extractedInfos;	
	}
	
	/**
	 * Gets loci where all individuals do not share the same alleles.
	 * 
	 * EG)  person1:  AA  person2:  AT  person3: AG  person4: AT
	 * @pre
	 * @post
	 * @param indiv
	 * @return  list of loci
	 */
	public static ArrayList<Locus> getLociDiffAlleles (ArrayList<VCFInfo> indivs, Genome genome)
	{
		ArrayList<Locus> lociDiffAlleles = new ArrayList<Locus>();
		ArrayList<Locus> allSortedLoci = genome.getSortedLoci();
		for (Locus locus: allSortedLoci)
		{
			AlleleInfo lastAlleles = indivs.get(0).getLocusAlleleInfo(locus);
			for (VCFInfo indiv : indivs)
			{
				AlleleInfo currAlleles = indiv.getLocusAlleleInfo(locus);
				if (currAlleles == null || currAlleles.getUnphasedAlleles().size() == 0)
				{
					continue;
				}
				
				if (lastAlleles != null && !currAlleles.equals(lastAlleles))
				{
					if (locus.getChromosome().equals("scf7180037884725") && locus.getPosition() ==	12619)
					{
						System.out.println("");
					}
					lociDiffAlleles.add(locus);
					break;
				}
				lastAlleles = indiv.getLocusAlleleInfo(locus);
			}
		}
		return lociDiffAlleles;
	}
}

